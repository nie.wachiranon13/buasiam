-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2021 at 05:42 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buabua`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `qtyperday` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `qtyperday`, `created_at`, `updated_at`) VALUES
(1, 'เสาโรมัน', 12, '2020-06-25 08:31:11', '2020-06-25 08:31:11'),
(2, 'คิ้วบัวปูนปั้น', 50, '2020-06-25 08:31:21', '2020-07-01 13:15:37'),
(3, 'หัวเสา', 10, '2020-06-29 02:25:31', '2020-06-29 02:25:31'),
(4, 'ลวดลาย', 1, '2020-07-01 15:20:14', '2020-07-01 15:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idcard` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `idcard`, `fname`, `lname`, `address`, `position`, `created_at`, `updated_at`) VALUES
(1, '1127454215467', 'สมชาย', 'ขยันมาก', '42/96 หมู่3 ต.บางชนะ อ.บางพัง จ.นนทบุรี', 'ช่างก่อ', '2020-06-24 19:02:10', '2020-06-24 19:02:10'),
(2, '1198522356214', 'สุชาติ', 'ดำขำ', '78/956 ต.บางสะพาน อ.เมือง จ.สมุทรสาคร', 'นายช่าง', '2020-06-24 19:03:18', '2020-06-24 19:03:18');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `color` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `totalprice` float NOT NULL,
  `status` text NOT NULL,
  `cus_name` varchar(100) NOT NULL,
  `cus_tel` varchar(20) NOT NULL,
  `slip` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `user_id`, `totalprice`, `status`, `cus_name`, `cus_tel`, `slip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 4000, 'ยืนยันการสั่งทำ', 'วชิรนนทื์', '0851261313', NULL, '2021-03-13 17:20:15', '2021-03-13 17:21:13', NULL),
(2, 6, 9500, 'ยืนยันการสั่งทำ', 'สุชาติ', '07867867868', NULL, '2021-03-13 17:31:11', '2021-03-13 18:15:15', NULL),
(3, 6, 2300, 'รอการยืนยัน', 'วชิรนนท์', '08212611313', NULL, '2021-03-14 10:11:57', '2021-03-14 10:11:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` bigint(20) NOT NULL,
  `productcustom_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `image` varchar(191) CHARACTER SET latin1 DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `detail` text CHARACTER SET latin1,
  `cost_cement` float DEFAULT NULL,
  `cost_soi` float DEFAULT NULL,
  `c_iron` float DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `total_price` float DEFAULT NULL,
  `other_price` float DEFAULT NULL,
  `note` longtext,
  `profit` float DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `productcustom_id`, `invoice_id`, `image`, `amount`, `detail`, `cost_cement`, `cost_soi`, `c_iron`, `cost`, `total_price`, `other_price`, `note`, `profit`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '/storage/product_custom/6AbIirSJ9E.JPG', 2, 'aaa', 59.5, 60, 550, 1069.5, 2000, NULL, NULL, 1330.5, '2021-03-13 17:20:15', '2021-03-13 17:21:13'),
(2, 3, 1, '/storage/product_custom/jsXtcQbkd3.jpg', 3, 'bbb', 87.5, 225, 66, 778.5, 1100, 900, 'ค่าแบบเพิ่มเติม', 1621.5, '2021-03-13 17:20:15', '2021-03-13 17:21:13'),
(3, 2, 2, '/storage/product_custom/wF8tfhXJdw.JPG', 2, 'aaa', 0, 225, 66, 691, 2300, 100, 'ค่าดักแปลงเสา', 2109, '2021-03-13 17:31:11', '2021-03-13 18:15:14'),
(4, 4, 2, '/storage/product_custom/dG2Mn0oAuO.jpg', 3, 'bbb', 0, 240, 440, 680, 6000, NULL, NULL, 5320, '2021-03-13 17:31:11', '2021-03-13 18:15:15'),
(5, 3, 2, '/storage/product_custom/wICgMyKq6d.jpg', 3, 'ccc', 0, 9, 11, 420, 1100, NULL, NULL, 1080, '2021-03-13 17:31:11', '2021-03-13 18:15:15'),
(6, 2, 3, '/storage/product_custom/XSNiJ4SbZr.JPG', 2, 'aaa', NULL, NULL, NULL, NULL, 2300, NULL, NULL, NULL, '2021-03-14 10:11:57', '2021-03-14 10:11:57');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`id`, `name`, `stock`, `created_at`, `updated_at`) VALUES
(1, 'ปูน', 1660, '2020-06-18 10:24:54', '2021-03-15 16:12:24'),
(2, 'ทราย', 61, '2020-06-18 10:25:22', '2021-03-13 18:15:15'),
(3, 'เหล็ก', 684.5, '2020-06-18 10:25:31', '2021-03-13 18:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2014_10_12_000000_create_users_table', 1),
(13, '2014_10_12_100000_create_password_resets_table', 1),
(14, '2019_08_19_000000_create_failed_jobs_table', 1),
(15, '2020_03_25_152009_create_categories_table', 1),
(16, '2020_03_31_183416_add_admin_field', 1),
(17, '2020_04_15_163419_create_orders_items', 1),
(18, '2020_04_16_075239_create_orders', 1),
(19, '2020_05_05_102637_create_productcustompillars_table', 1),
(20, '2020_05_27_125548_create_events_table', 1),
(21, '2020_05_29_110758_create_stocks_table', 1),
(22, '2020_06_01_161637_create_products_table', 1),
(23, '2020_06_02_182624_create_works_table', 2),
(24, '2020_06_02_195329_create_jobs_table', 3),
(25, '2020_06_17_160050_create_materials_table', 4),
(26, '2020_06_18_150256_create_stockmaterials_table', 5),
(27, '2020_06_18_151047_create_stock_m_table', 6),
(28, '2020_06_18_173140_create_stock_material_table', 7),
(29, '2020_06_18_173605_create_stock_materials_table', 8),
(30, '2020_06_18_192349_create_employees_table', 9),
(31, '2020_08_09_234438_create_slips_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_price` decimal(8,2) NOT NULL,
  `item_amount` int(11) NOT NULL,
  `cost` decimal(8,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`id`, `item_id`, `order_id`, `item_name`, `item_price`, `item_amount`, `cost`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'เสาโรมันหน้าขนาด 20 ซม.', '600.00', 2, '462.00', '2021-01-27 05:21:59', '2021-01-27 05:21:59'),
(2, 1, 2, 'เสาโรมันหน้าขนาด 20 ซม.', '600.00', 8, '1848.00', '2021-02-09 12:54:52', '2021-02-09 12:54:53'),
(3, 2, 3, 'คิ้วบัวขนาด 10 ซม', '100.00', 2, '90.00', '2021-03-14 12:49:45', '2021-03-14 12:49:45'),
(4, 2, 5, 'คิ้วบัวขนาด 10 ซม', '100.00', 1, '45.00', '2021-03-15 15:17:08', '2021-03-15 15:17:08');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `status` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slip` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `price`, `status`, `fname`, `lname`, `address`, `phone`, `zip`, `email`, `slip`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '1200.00', 'ชำระเงินแล้ว', 'คนสั่ง', 'สั่ง', 'thailand', '0851261313', '11120', 'ninepare@hotmail.com', NULL, 6, '2021-01-27 05:21:59', '2021-03-10 13:41:28'),
(2, '4800.00', 'ชำระเงินแล้ว', 'ไดร์ฟ', 'อิอิ', 'thailand', '0851261313', '11120', 'drive01@hotmail.com', NULL, 13, '2021-02-09 12:54:52', '2021-03-11 12:58:15'),
(3, '200.00', 'ยังไม่ชำระเงิน', 'คนซื้อ', '', '42/66', '0851261313', '111120', 'ninepare@hotmail.com', 'slips/SuQeSDiZty.jpg', 6, '2021-03-14 12:49:44', '2021-03-15 16:00:09'),
(5, '100.00', 'ยังไม่ชำระเงิน', 'คนซื้อ', '', '42/66', '0851261313', '111120', 'ninepare@hotmail.com', NULL, 6, '2021-03-15 15:17:08', '2021-03-15 15:17:08');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('nie.waciranon@hotmail.com', '$2y$10$8w2BzL.g4/vl65i/4yW/F.UPSoJMv53Dr6TeI6zcha9RqJI3OTtdu', '2020-07-07 05:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `productcustompillars`
--

CREATE TABLE `productcustompillars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nameorder` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `length` int(11) DEFAULT NULL,
  `width` int(11) NOT NULL,
  `high` int(11) NOT NULL,
  `long` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `cement_price` float DEFAULT NULL,
  `soi_price` float DEFAULT NULL,
  `iron_price` float DEFAULT NULL,
  `work_price` int(11) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'C',
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `productcustompillars`
--

INSERT INTO `productcustompillars` (`id`, `nameorder`, `name`, `tel`, `description`, `image`, `price`, `length`, `width`, `high`, `long`, `amount`, `cement_price`, `soi_price`, `iron_price`, `work_price`, `start`, `end`, `status`, `mode`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'หัวเสา', 'วชิรนนท์', '0851261313', 'รูปแบบที่1', 'XzE2MDk3NDM4NDY=.jpg', '6500.00', 1, 20, 25, 15, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'รอการมอบหมายงาน', 'C', 4, '2021-01-04 07:04:07', '2021-01-04 07:04:07'),
(2, 'เสาโรมันหน้าขนาด 20 ซม.', 'wachiranon', '-', '-', 'XzE1OTM4Njg2OTY=.JPG', '6000.00', 10, 20, 0, 0, 1, 510, 800, 500, 500, NULL, NULL, 'รอการมอบหมายงาน', 'A', 1, '2021-01-27 05:21:03', '2021-01-27 05:21:03'),
(3, 'เสาโรมันหน้าขนาด 20 ซม.จ้าาาาา', 'wachiranon', '-', '-', 'XzE1OTM4Njg2OTY=.JPG', '600.00', 1, 20, 0, 0, 1, 51, 80, 50, 50, NULL, NULL, 'รอการมอบหมายงาน', 'A', 1, '2021-03-03 04:49:49', '2021-03-03 04:49:49'),
(4, 'หัวเสา', 'wachiranon', '-', '-', 'XzE1OTM4NzU0OTQ=.JPG', '180.00', 1, 12, 0, 0, 1, 15, 30, 20, 30, NULL, NULL, 'รอการมอบหมายงาน', 'A', 3, '2021-03-03 15:31:11', '2021-03-03 15:31:11'),
(5, 'เสาโรมันหน้าขนาด 20 ซม.', 'wachiranon', '-', '-', 'XzE1OTM4Njg2OTY=.JPG', '12000.00', 20, 20, 0, 0, 1, 1020, 1600, 1000, 1000, NULL, NULL, 'รอการมอบหมายงาน', 'A', 1, '2021-03-10 08:37:51', '2021-03-10 08:37:51'),
(6, 'เสาโรมันขนาด 20 ซม.', 'wachiranon', '-', '-', 'XzE2MTUzOTE4NTg=.JPG', '6000.00', 10, 20, 0, 0, 1, NULL, NULL, 500, 500, NULL, NULL, 'รอการมอบหมายงาน', 'A', 1, '2021-03-10 16:00:55', '2021-03-10 16:00:55'),
(7, 'คิ้วบัวขนาด 10 ซม', 'wachiranon', '-', '-', 'XzE2MTUzOTE5MjQ=.JPG', '1000.00', 10, 10, 0, 0, 1, 105, 120, 100, 125, NULL, NULL, 'รอการมอบหมายงาน', 'A', 2, '2021-03-10 16:01:21', '2021-03-10 16:01:21'),
(8, 'เสาโรมันขนาด 20 ซม.', 'wachiranon', '-', '-', 'XzE2MTUzOTE4NTg=.JPG', '6000.00', 10, 20, 0, 0, 1, 595, 800, 500, 500, NULL, NULL, 'รอการมอบหมายงาน', 'A', 1, '2021-03-10 16:04:33', '2021-03-10 16:04:33');

-- --------------------------------------------------------

--
-- Table structure for table `productcustoms`
--

CREATE TABLE `productcustoms` (
  `productcustom_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET tis620 COLLATE tis620_bin NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `image` varchar(191) CHARACTER SET latin1 DEFAULT NULL,
  `width` int(11) NOT NULL,
  `length` int(11) DEFAULT NULL,
  `heigth` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `cement` float NOT NULL,
  `soi` float NOT NULL,
  `iron` float NOT NULL,
  `workprice` float NOT NULL,
  `mold` float NOT NULL,
  `mold_cost` float NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `productcustoms`
--

INSERT INTO `productcustoms` (`productcustom_id`, `name`, `description`, `image`, `width`, `length`, `heigth`, `price`, `category_id`, `cement`, `soi`, `iron`, `workprice`, `mold`, `mold_cost`, `created_at`, `updated_at`) VALUES
(1, 'เสาโรมันขนาด 20 ซม.', 'รูปแบบที่1', NULL, 20, NULL, NULL, 600, 1, 17, 20, 25, 50, 800, 400, '2021-03-09 14:55:45', '2021-03-09 14:55:45'),
(2, 'เสาโรมันขนาด 25 ซม.', 'รูปแบบที่1', NULL, 25, NULL, NULL, 750, 1, 25, 75, 3, 50, 800, 400, '2021-03-09 15:15:05', '2021-03-09 15:15:05'),
(3, 'คิ้วบัว', 'รูปแบบที่1', NULL, 10, NULL, NULL, 100, 2, 3, 3, 0.5, 12.5, 800, 400, '2021-03-09 15:16:33', '2021-03-09 15:16:33'),
(4, 'รูปปั้น', 'รูปแบบที่1', NULL, 30, 35, 40, 2000, 4, 50, 80, 20, 100, 0, 0, '2021-03-10 16:49:07', '2021-03-10 16:49:07');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `cement` float NOT NULL,
  `soi` float NOT NULL,
  `iron` float NOT NULL,
  `workprice` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `image`, `width`, `price`, `category_id`, `stock`, `cement`, `soi`, `iron`, `workprice`, `created_at`, `updated_at`) VALUES
(1, 'เสาโรมันขนาด 20 ซม.', 'aaa', 'OpFGIUyzWY.jpg', 20, 0, 1, 10, 17, 20, 2.5, 50, '2021-03-10 15:57:38', '2021-03-15 16:59:47'),
(2, 'คิ้วบัวขนาด 10 ซม', 'xxx', 'XzE2MTUzOTE5MjQ=.JPG', 10, 100, 2, 7, 3, 3, 0.5, 12.5, '2021-03-10 15:58:44', '2021-03-15 15:17:08');

-- --------------------------------------------------------

--
-- Table structure for table `slips`
--

CREATE TABLE `slips` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `orderid` int(11) NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `lot_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `balance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `cement_price` float DEFAULT NULL,
  `soi_price` float DEFAULT NULL,
  `iron_price` float DEFAULT NULL,
  `work_price` float DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`lot_id`, `product_id`, `quantity`, `status`, `balance`, `created_at`, `cement_price`, `soi_price`, `iron_price`, `work_price`, `updated_at`) VALUES
(2, 2, 10, 0, 7, '2021-03-10 16:01:20', 105, 120, 100, 125, '2021-03-15 15:17:08'),
(3, 1, 10, 0, 10, '2021-03-10 16:04:33', 595, 800, 500, 500, '2021-03-10 16:04:33'),
(4, 1, 20, 0, 20, '2021-03-15 16:12:24', NULL, NULL, NULL, NULL, '2021-03-15 16:12:24'),
(5, 1, 20, 0, 20, '2021-03-15 16:15:13', NULL, NULL, NULL, NULL, '2021-03-15 16:15:13'),
(6, 1, 20, 0, 20, '2021-03-15 16:17:12', NULL, NULL, NULL, NULL, '2021-03-15 16:17:12'),
(7, 1, 20, 0, 20, '2021-03-15 16:18:43', NULL, NULL, NULL, NULL, '2021-03-15 16:18:43'),
(8, 1, 20, 0, 20, '2021-03-15 16:19:54', NULL, NULL, NULL, NULL, '2021-03-15 16:19:54'),
(9, 1, 20, 0, 20, '2021-03-15 16:20:29', NULL, NULL, NULL, NULL, '2021-03-15 16:20:29'),
(10, 1, 20, 0, 20, '2021-03-15 16:21:43', NULL, NULL, NULL, NULL, '2021-03-15 16:21:43'),
(11, 1, 20, 0, 20, '2021-03-15 16:21:57', NULL, NULL, NULL, NULL, '2021-03-15 16:21:57'),
(12, 1, 20, 0, 20, '2021-03-15 16:23:20', NULL, NULL, NULL, NULL, '2021-03-15 16:23:20'),
(13, 1, 20, 0, 20, '2021-03-15 16:28:25', NULL, NULL, NULL, NULL, '2021-03-15 16:28:25'),
(14, 1, 20, 0, 20, '2021-03-15 16:31:45', NULL, NULL, NULL, NULL, '2021-03-15 16:31:45'),
(15, 1, 20, 0, 20, '2021-03-15 16:32:32', NULL, NULL, NULL, NULL, '2021-03-15 16:32:32');

-- --------------------------------------------------------

--
-- Table structure for table `stock_materials`
--

CREATE TABLE `stock_materials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `material_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `quantity` float NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `balance` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stock_materials`
--

INSERT INTO `stock_materials` (`id`, `material_id`, `price`, `quantity`, `status`, `balance`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 100, 1, 0, '2021-03-10 15:59:24', '2021-03-10 16:00:54'),
(2, 1, 3.5, 1900, 1, 0, '2021-03-10 15:59:35', '2021-03-13 18:10:08'),
(3, 2, 3.5, 100, 1, 0, '2021-03-10 15:59:56', '2021-03-10 16:00:55'),
(4, 2, 4, 1900, 1, 0, '2021-03-10 16:00:08', '2021-03-11 17:39:50'),
(5, 3, 20, 100, 1, 0, '2021-03-10 16:00:23', '2021-03-10 16:08:16'),
(6, 3, 22, 1900, 0, 684.5, '2021-03-10 16:00:32', '2021-03-13 18:15:15'),
(7, 2, 3, 100, 1, 0, '2021-03-13 17:04:03', '2021-03-13 17:05:06'),
(8, 2, 3, 1000, 0, 61, '2021-03-13 17:07:02', '2021-03-13 18:15:15'),
(9, 1, 3.5, 50, 1, 0, '2021-03-13 17:43:17', '2021-03-13 18:11:19'),
(10, 1, 3, 2000, 0, 1660, '2021-03-14 08:31:10', '2021-03-15 16:12:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `isAdmin`, `email`, `email_verified_at`, `password`, `remember_token`, `address`, `postal`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'wachiranon', 1, 'nie.waciranon@hotmail.com', NULL, '$2y$10$6xJaMq.Dpu3R1NobpaaYJumfa6j.8c7tJsYPk7WHByNaEkM0twgc.', NULL, '', '', '', '2020-06-01 09:25:14', '2020-06-01 09:25:14'),
(6, 'คนซื้อ', 0, 'ninepare@hotmail.com', NULL, '$2y$10$Tv.FI6ar.DTabdSU.JyjaeucaAHnYZqO6hEKvm7w7ZvXpc.pjZQv6', '9uRFJNTQ5ct7hahJguxeAGMcjBfbCUrgn9Ed7QrgtgBc6C9iIIghdpxhmuGx', '42/66', '111120', '0851261313', '2020-06-09 08:31:15', '2020-06-09 08:31:15'),
(7, 'สมพง', 0, 'sompong@hotmail.com', NULL, '$2y$10$hDKUJoyIACj8BMDTD.H97uEf2HEaoUy8e/X.43ewJYGVe3WtsKU0O', NULL, '', '', '', '2020-06-25 08:43:47', '2020-06-25 08:43:47'),
(11, 'สุชาต ขยันมาก', 0, 'mom@hotmail.com', NULL, '$2y$10$IOBxp/GcKGenNuOw1Yc5yegUVoIRI5jpnT6UqM/EnO2TIRDbcr8.e', NULL, '42/96 หมู่3', '11120', '0851261313', '2020-08-03 07:52:05', '2020-08-03 07:52:05'),
(12, 'น้องนาย หล่อจัง', 0, 'nine@hotmail.com', NULL, '$2y$10$O6YTi9YX54nHu1H7w4XZkOVYAM5twNWnkd2xATIKKCtPRICULZE.S', NULL, '42/464 หมู่3', '11120', '0851261313', '2020-08-10 04:22:12', '2020-08-10 04:22:12'),
(13, 'drive', 0, 'drive@hotmail.com', NULL, '$2y$10$ryTnzUhWuAm60wOVnThzHemnveFuS4HNwBBxQqTfG5Y06OC.rzONu', NULL, '42/5464', '11120', '0851261313', '2021-02-09 12:38:21', '2021-02-09 12:38:21'),
(14, 'drive01', 0, 'drive01@hotmail.com', NULL, '$2y$10$qLgNvbartc74DQtAmsqzCOBTOwphpWDVsnlVh4Jo5JCZNUiLqgwhS', NULL, 'thailand', '11120', '0851261313', '2021-02-09 12:41:20', '2021-02-09 12:41:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`,`user_id`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `productcustompillars`
--
ALTER TABLE `productcustompillars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productcustoms`
--
ALTER TABLE `productcustoms`
  ADD PRIMARY KEY (`productcustom_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`);

--
-- Indexes for table `slips`
--
ALTER TABLE `slips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`lot_id`);

--
-- Indexes for table `stock_materials`
--
ALTER TABLE `stock_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `productcustompillars`
--
ALTER TABLE `productcustompillars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `productcustoms`
--
ALTER TABLE `productcustoms`
  MODIFY `productcustom_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slips`
--
ALTER TABLE `slips`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `lot_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `stock_materials`
--
ALTER TABLE `stock_materials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
