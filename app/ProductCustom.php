<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCustom extends Model
{
    public $table = "productcustoms";

    protected $primaryKey = 'productcustom_id';
}
