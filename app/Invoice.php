<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    protected $appends = ['ref_id'];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
    ];

    public function getRefIdAttribute()
    {
        return  'INVOICE' . str_pad($this->id, 6, '0', STR_PAD_LEFT);;
    }

    public function invoiceDetails()
    {
        return $this->hasOne(InvoiceDetail::class);
    }

    public function invoiceDetailAll()
    {
        return $this->hasMany(InvoiceDetail::class);
    }
}
