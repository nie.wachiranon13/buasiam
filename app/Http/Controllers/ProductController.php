<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Category;
use App\Product;
use App\productcustompillar;
use App\Cart;
use App\Order;
use App\Orderitem;
use App\Stock;
use App\ProductCustom;
use App\User;
use App\Slip;
use App\Employee;
use App\Http\Controllers\Admin\StockController;
use App\Invoice;
use App\InvoiceDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\UploadController;

class ProductController extends Controller
{
    public function index()
    {

        $cart = Session::get('cart');
        return view("products.showProduct", ['cartItems' => $cart])
            ->with("products", Product::paginate(6))
            ->with("categories", Category::all()->sortBy('name'));
    }
    public function test()
    {

        return view("products.testcal");
    }

    public function findCategory($id)
    {
        $category = Category::find($id);
        return view("products.findProduct")
            ->with("categories", Category::all()->sortBy('name'))
            ->with("products", $category->products()->paginate(3))
            ->with('feature', $category->name);
    }
    public function details($id)
    {

        return view("products.showProductdetails")
            ->with("product", Product::find($id))
            ->with("categories", Category::all()->sortBy('name'));
    }

    public function addProductToCart(Request $request, $id)
    {

        $product = Product::find($id);

        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $cart->addItem($id, $product);


        $request->session()->put('cart', $cart);

        return redirect('/products/cart');
        // $request->session()->forget('cart');
    }


    public function addQuantityToCart(Request $request)
    {
        $id = $request->_id;
        $quantity = $request->quantity;

        $product = Product::find($id);

        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $cart->addQuantity($id, $product, $quantity);
        //$cart->addItem($id,$product);
        $cart->updatePriceQuantity();


        $request->session()->put('cart', $cart);

        return redirect('/products/cart');
    }

    public function showCart()
    {
        $cart = Session::get('cart');
        if ($cart) {

            return view('products.showCart', ['cartItems' => $cart])->with("categories", Category::all()->sortBy('name'));
        } else {
            return redirect('/products')->with("categories", Category::all()->sortBy('name'));
        }
    }
    public function deleteFromCart(Request $request, $id)
    {
        $cart = $request->session()->get('cart');

        //เช็คสินค้าในcart ว่าคีย์ตรงไหม
        if (array_key_exists($id, $cart->items)) {

            unset($cart->items[$id]);
        }

        //เอาสินค้าคงเหลือลงมาทำงาน
        $afterCart = $request->session()->get('cart');
        $updateCart = new Cart($afterCart);
        $updateCart->updatePriceQuantity();

        $request->session()->put('cart', $updateCart);
        return redirect('/products/cart');
    }

    public function incrementCart(Request $request, $id)
    {

        $product = Product::find($id);

        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $cart->addItem($id, $product);


        $request->session()->put('cart', $cart);

        return redirect('/products/cart');
    }

    public function decrementCart(Request $request, $id)
    {

        $product = Product::find($id);
        $prevCart = $request->session()->get('cart');
        $cart = new cart($prevCart);
        //เช็ค quantity
        if ($cart->items[$id]['quantity'] > 1) {

            $cart->items[$id]['quantity'] = $cart->items[$id]['quantity'] - 1;

            $cart->items[$id]['totalSinglePrice'] = $cart->items[$id]['quantity'] * $product['price'];
            $cart->updatePriceQuantity();
            $request->session()->put('cart', $cart);
        }
        return redirect('/products/cart');
    }

    public function searchProduct(Request $request)
    {
        $name = $request->search;
        $products = Product::where('name', "LIKE", "%{$name}%")->paginate(3);

        return view("products.searchProduct")
            ->with("products", $products)
            ->with("categories", Category::all()->sortBy('name'));
    }
    public function checkout()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        return view('products.checkoutPage')
            ->with(
                "categories",
                Category::all()->sortBy('name')
            );
    }


    public function createOrder(Request $request)
    {

        //เช็คจำนวนคงคลังสินค้า
        $cart = Session::get('cart');

        foreach ($cart->items as $item) {

            $item_id = $item['data']['id'];
            $item_quantity = $item['quantity'];
            $product =  Product::find($item_id);
            //เช็คของว่าพอหรือไหม
            if ($product->stock < $item_quantity) {
                //return back
                return redirect()->back()->with('error', 'สินค้าไม่เพียงพอเนืองจาก ' . $product->name . 'มีสินค้าเพียง ' . $product->stock . ' รายการ');
            }
            //ลด stock สินค้า
            $product = Product::find($item_id)->decrement('stock', $item_quantity);
        }

        //เพิ่ม order
        $order = new Order();
        $order->price = $cart->totalPrice;
        $order->status = "ยังไม่ชำระเงิน";
        $order->fname = $request->fname;
        // $order->lname = $request->lname;
        $order->address = $request->address;
        $order->phone = $request->phone;
        $order->zip = $request->zip;
        $order->email = $request->email;
        $order->user_id = Auth::id();
        $order->save();
        $last_id =  $order->order_id;

        $newOrder = array(
            "date" => now(),
            "price" => $cart->totalPrice,
            "status" => "ยังไม่ชำระเงิน",
            "del_date" => now(),
            "fname" => $request->fname,
            // "lname" => $request->lname,
            "address" => $request->address,
            "email" => $request->email,
            "zip" => $request->zip,
            "phone" => $request->phone,
            "user_id" => Auth::id()
        );

        if ($last_id) {
            //order detail
            foreach ($cart->items as $item) {
                $item_id = $item['data']['id'];
                $item_name = $item['data']['name'];
                $item_price = $item['data']['price'];
                $item_amount = $item['quantity'];

                $newOrderItem =  new Orderitem;
                $newOrderItem->item_id = $item_id;
                $newOrderItem->order_id = $last_id;
                $newOrderItem->item_name = $item_name;
                $newOrderItem->item_price = $item_price;
                $newOrderItem->item_amount = $item_amount;
                $newOrderItem->cost = 0;

                if ($newOrderItem->save()) {
                    $item_detail_order_id =  $newOrderItem->id;
                }
                //คำนวณ
                $cost = $this->caculate($last_id, $item_id, $item_detail_order_id, $item_amount);
            }

            Session::forget("cart");
            $payment_info = $newOrder;
            $payment_info["order_id"] = $last_id;
            $request->session()->put("payment_info", $payment_info);

            return redirect('/products/showPayment');
        }
    }

    public function caculate($order_id, $item_id, $order_detail_id, $item_amount)
    {
        //คำนวณต้นทุนรายก่ีแต่ละสินค้า
        $stock = Stock::where('status', 0)->where('product_id', $item_id)->orderBy('created_at', 'ASC')->first();
        //คำนวณ
        if (($stock->balance - $item_amount) >= 0) { //b = 30 - item = 5 = 25
            //5 *  (40 + 50 + 100) = 950
            $cost  =  $item_amount * (($stock->cement_price + $stock->soi_price + $stock->iron_price + $stock->work_price) / $stock->quantity);
            //เก็บ

            Stock::find($stock->lot_id)->decrement('balance', $item_amount);

            if (($stock->balance - $item_amount) == 0) {

                $save_stock  = Stock::find($stock->lot_id);
                $save_stock->balance = 0;
                $save_stock->status = 1;
                $save_stock->save();
            }

            Orderitem::find($order_detail_id)->increment('cost', $cost);

            return $cost;
        } else {
            //ถ้าติดลบ 30,35
            $item_l = ($stock->balance - $item_amount) * -1; //2 -3 = -1

            $save_stock  = Stock::find($stock->lot_id);

            $cost =  $save_stock->balance * (($stock->cement_price + $stock->soi_price + $stock->iron_price + $stock->work_price) / $stock->quantity);

            $save_stock->balance = 0;
            $save_stock->status = 1;
            $save_stock->save();

            //เก็บ

            Orderitem::find($order_detail_id)->increment('cost', $cost);

            $this->caculate($order_id, $item_id, $order_detail_id, $item_l);
        }
    }

    public function createOrder_bk(Request $request)
    {

        $cart = Session::get('cart');
        $email = $request->email;
        $fname = $request->fname;
        $lname = $request->lname;
        $address = $request->address;
        $zip = $request->zip;
        $phone = $request->phone;
        $user_id = Auth::id();

        if ($cart) {
            $date = date("Y-m-d H:i:s");
            //Data
            $newOrder = array(
                "date" => $date,
                "price" => $cart->totalPrice,
                "status" => "ยังไม่ชำระเงิน",
                "del_date" => $date,
                "fname" => $fname,
                "lname" => $lname,
                "address" => $address,
                "email" => $email,
                "zip" => $zip,
                "phone" => $phone,
                "user_id" => $user_id
            );
            // Insert Order Data
            $create_Order = DB::table('orders')->insert($newOrder);
            $order_id = DB::getPDO()->lastInsertId();

            // Insert Order Item Data
            foreach ($cart->items as $item) {
                $item_id = $item['data']['id'];
                $item_name = $item['data']['name'];
                $item_price = $item['data']['price'];
                $item_amount = $item['quantity'];
                $newOrderItem = array(
                    "item_id" => $item_id,
                    "order_id" => $order_id,
                    "item_name" => $item_name,
                    "item_price" => $item_price,
                    "item_amount" => $item_amount,
                );
                $create_orderItem = DB::table("orderitems")->insert($newOrderItem);


                $getProductStock = Product::where('id', $item_id)->first();


                $newStock = $getProductStock->stock - $item_amount;

                Product::where('id', $item_id)->update(['stock' => $newStock]);
            }



            Session::forget("cart");
            $payment_info = $newOrder;
            $payment_info["order_id"] = $order_id;
            $request->session()->put("payment_info", $payment_info);
            return redirect('/products/showPayment');
            // echo "Original stock: ".$getProductStock->stock;
            // echo "Stock reduce: ".$item_amount;
            // echo "new stock: ".$newStock;
        } else {
            return redirect('/products');
        }
    }

    public function showOrderdetail($id)
    {

        $orderitems = DB::table('orders')
            ->join('orderitems', 'orders.order_id', '=', 'orderitems.order_id')
            ->where('orders.order_id', $id)
            ->get();
        return view('products.orderdetail', ["orderitems" => $orderitems])
            ->with("categories", Category::all()->sortBy('name'));
    }


    public function showPayment()
    {
        $payment_info  = Session::get('payment_info');
        if ($payment_info['status'] == 'ยังไม่ชำระเงิน') {
            return view("payment.paymentPage", ["payment_info" => $payment_info])->with("categories", Category::all()->sortBy('name'));
        } else {

            return  redirect('/products')->with("categories", Category::all()->sortBy('name'));
        }
    }


    public function createCustom()
    {

        $product_customs = ProductCustom::all();

        return view(
            'products.createCustomForm',
            compact(
                'product_customs'
            )
        )->with("categories", Category::all()->sortBy('name'));
    }

    //custom นาย
    public function orderCustomStore(Request $request)
    {

        $product_list = [];
        $check_stock = new StockController();
        $upload = new UploadController();

        $totalprice = 0;
        foreach ($request->productcustom_id as $key => $product_cus_id) {

            $product = ProductCustom::where('productcustom_id', $product_cus_id)->first();

            /*
            $product_list[$key] = [
                'productcustom_id' => $product_cus_id, // รหัสินค้าสั่งซื้อ
                'image' =>   $upload->uploadeImage($request->product_custom_image[$key]), // รูปที่ลูกค้าส่งมา
                'amount' => $request->quantity[$key], // จำนวนที่สั่ง
                'cost_cement' =>  $c_cement, // ค่าcost ของทรายทั้งหมด
                'cost_soi' =>  $c_soi,
                'c_iron' =>  $c_iron,
                'cost' => $c_cement + $c_soi +  $c_iron + $product->mold_cost, //ค่าต้นทุน
                'total_price' => ($product->price *  $request->quantity[$key]) + $product->mold, // ราคาขาย
                'profit' => (($product->price *  $request->quantity[$key]) + $product->mold) - ($c_cement + $c_soi +  $c_iron + $product->mold_cost), // กำไร
                'detail' => $request->detail[$key]
            ];
            */
            $image =  !empty($request->product_custom_image[$key]) ? $upload->uploadeImage($request->product_custom_image[$key]) : null;

            $product_list[$key] = [
                'productcustom_id' => $product_cus_id, // รหัสินค้าสั่งซื้อ
                'image' =>  $image, // รูปที่ลูกค้าส่งมา
                'amount' => $request->quantity[$key], // จำนวนที่สั่ง
                'total_price' => ($product->price *  $request->quantity[$key]) + $product->mold, // ราคาขาย
                'detail' => $request->detail[$key]
            ];

            $totalprice +=  ($product->price *  $request->quantity[$key]) + $product->mold;
        }

        $invoice = new Invoice();
        $invoice->user_id = Auth::user()->id;
        $invoice->totalprice = $totalprice;
        $invoice->status = 'รอการยืนยัน';
        $invoice->cus_name =  $request->cus_name;
        $invoice->cus_tel = $request->cus_tel;

        if ($invoice->save()) {
            $invoice->invoiceDetails()->createMany($product_list);
        }

        return redirect('/products/thank');
    }

    public function checkstatuspayment()
    {

        $orders = DB::table('orders')
            ->where('user_id', Auth::user()->id)
            ->paginate(10);


        $invoices = DB::table('invoices')
            ->where('user_id', Auth::user()->id)

            ->paginate(10);

        return view('products.checkstatuspayment', ["orders" => $orders], ["invoices" => $invoices])

            ->with("categories", Category::all()->sortBy('name'));
    }



    public function thank()
    {
        return view('products.thank')->with("categories", Category::all()->sortBy('name'));
    }

    public function slip()

    {

        $orders = DB::table('orders')
            ->where('user_id', Auth::user()->id)
            ->where('status', "ยังไม่ชำระเงิน")
            ->paginate(10);


        $invoices = DB::table('invoices')
            ->where('user_id', Auth::user()->id)
            ->where('status', "ยืนยันการสั่งทำ")

            ->paginate(10);


        return view('products.slip', ["orders" => $orders], ["invoices" => $invoices])

            ->with("categories", Category::all()->sortBy('name'));
    }

    //สลิป


    public function slipOrder($id)

    {
        $orders = Order::find($id);

        $id = $id;

        return view(
            'products.sliporder',
            compact('id')
        )->with('orders', $orders)
            ->with("categories", Category::all()->sortBy('name'));
    }
    public function slipCustom($id)
    {
        $invoices = Invoice::find($id);
        return view("products.slipcustom")->with('invoices', $invoices)
            ->with("categories", Category::all()->sortBy('name'));
    }

    public function sendslipOrder(Request $request, $id)
    {

        $orders = Order::find($id);
        $request->validate([

            'slip' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',

        ]);

        $uplad = new UploadController();
        $slip = $uplad->uploadeFile($request->slip, 'slips');

        $orders->slip =  $slip;
        $orders->save();

        return redirect('/products/confirmslip');
    }
    public function sendslipCustom(Request $request, $id)
    {

        $request->validate([

            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',


        ]);
        $stringImageReFormat = base64_encode('_' . time());
        $ext = $request->file('image')->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request->image);

        Storage::disk('local')->put('public/create_custom_image/' . $imageName, $imageEncoded);

        $slip = new Invoice;
        $slip->image = $imageName;
        $slip->save();
        return redirect('/products/confirmslip');
    }

    public function confirmslip()
    {
        return view('products.confirmslip')->with("categories", Category::all()->sortBy('name'));
    }
    public function sendslip(Request $request)
    {
        $request->validate([

            'price' => 'required|numeric',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',


        ]);
        $stringImageReFormat = base64_encode('_' . time());
        $ext = $request->file('image')->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request->image);

        Storage::disk('local')->put('public/create_custom_image/' . $imageName, $imageEncoded);
        $slip = new Slip;
        $slip->orderid = $request->orderid;
        $slip->price = $request->price;
        $slip->image = $imageName;
        $slip->save();
        return redirect('/products/confirmslip');
    }
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'tel' => 'required|numeric',


            'price' => 'required|numeric',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',


        ]);

        $stringImageReFormat = base64_encode('_' . time());
        $ext = $request->file('image')->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request->image);

        Storage::disk('local')->put('public/create_custom_image/' . $imageName, $imageEncoded); //กำหนดค่าปลายทางที่เก็บรูปภาพ
        $product = new productcustompillar;
        $product->name = $request->name;
        $product->tel = $request->tel;

        $product->description = $request->description;
        // $product->category_id = $request->category;
        $product->price = $request->price;
        //$product->stock = $request->stock;
        $product->image = $imageName;
        $product->save();

        Session()->flash("success", "บันทึกข้อมูลสำเร็จ!");
        return redirect('/products/createCustom');
    }

    public function createPillar()
    {

        Session()->flash("success", "บันทึกข้อมูลสำเร็จ!");
        return view('products.pillar')->with("categories", Category::all()->sortBy('name'));
    }
    public function storePillar(Request $request)
    {


        $request->validate([

            'name' => 'required',

            'tel' => 'required|numeric',

            'price' => 'required|numeric',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',


        ]);

        $stringImageReFormat = base64_encode('_' . time());
        $ext = $request->file('image')->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request->image);

        Storage::disk('local')->put('public/create_custom_image/' . $imageName, $imageEncoded); //กำหนดค่าปลายทางที่เก็บรูปภาพ
        $product = new productcustompillar;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->tel = $request->tel;
        $product->price = $request->price;
        $product->category_id = 1;
        $product->image = $imageName;
        $product->nameorder = $request->nameorder;
        $product->width = $request->width;
        $product->length = $request->length;
        $product->amount = $request->amount;
        $product->status = $request->status;


        $product->save();


        $diffDate = $this->diffDateOfwork(1, $request->length * $request->amount);

        return redirect('/products/thank')->with('diff_date', $diffDate);
    }
    public function createCutebua()
    {
        return view('products.cutebua')->with("categories", Category::all()->sortBy('name'));
    }
    public function createHeadpillar()
    {
        return view('products.headpillar')->with("categories", Category::all()->sortBy('name'));
    }
    public function storeCutebua(Request $request)
    {
        $request->validate([

            'name' => 'required',


            'price' => 'required|numeric',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',


        ]);

        $stringImageReFormat = base64_encode('_' . time());
        $ext = $request->file('image')->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request->image);

        Storage::disk('local')->put('public/create_custom_image/' . $imageName, $imageEncoded); //กำหนดค่าปลายทางที่เก็บรูปภาพ
        $product = new productcustompillar;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->tel = $request->tel;
        $product->price = $request->price;
        $product->category_id = 2;
        $product->image = $imageName;
        $product->nameorder = $request->nameorder;
        $product->width = $request->width;
        $product->length = $request->length;
        $product->amount = $request->amount;
        $product->status = $request->status;

        $product->save();

        $diffDate = $this->diffDateOfwork(2, $request->length * $request->amount);

        // Session()->flash(["success" => "บันทึกข้อมูลสำเร็จ!","diff_date" => $diffDate]);
        return redirect('/products/thank')->with('diff_date', $diffDate);
    }

    public function storeHead(Request $request)
    {
        $request->validate([

            'name' => 'required',

            'tel' => 'required|numeric',

            'price' => 'required|numeric',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',


        ]);

        $stringImageReFormat = base64_encode('_' . time());
        $ext = $request->file('image')->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request->image);

        Storage::disk('local')->put('public/create_custom_image/' . $imageName, $imageEncoded); //กำหนดค่าปลายทางที่เก็บรูปภาพ
        $product = new productcustompillar;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->tel = $request->tel;
        $product->price = $request->price;
        $product->category_id = 3;
        $product->image = $imageName;
        $product->nameorder = $request->nameorder;
        $product->width = $request->width;

        $product->amount = $request->amount;
        $product->status = $request->status;


        $product->save();


        $diffDate = $this->diffDateOfwork(3, $request->amount);

        // Session()->flash(["success" => "บันทึกข้อมูลสำเร็จ!","diff_date" => $diffDate]);
        return redirect('/products/thank')->with('diff_date', $diffDate);
    }

    public function create3d()
    {


        return view('products.3dbua')->with("categories", Category::all()->sortBy('name'));
    }
    public function store3d(Request $request)
    {
        $request->validate([

            'name' => 'required',

            'tel' => 'required|numeric',

            'price' => 'required|numeric',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',


        ]);

        $stringImageReFormat = base64_encode('_' . time());
        $ext = $request->file('image')->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request->image);

        Storage::disk('local')->put('public/create_custom_image/' . $imageName, $imageEncoded);
        $product = new productcustompillar;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->tel = $request->tel;
        $product->price = $request->price;
        $product->category_id = 4;
        $product->image = $imageName;
        $product->nameorder = $request->nameorder;
        $product->width = $request->width;
        $product->long = $request->long;
        $product->length = 1;
        $product->high = $request->high;
        $product->amount = $request->amount;
        $product->status = $request->status;

        $product->save();

        $diffDate = $this->diffDateOfwork(4, $request->amount);

        //   Session()->flash(["success" => "บันทึกข้อมูลสำเร็จ!","diff_date" => $diffDate]);

        return redirect('/products/thank')->with('diff_date', $diffDate);
    }
    public function diffDateOfwork($cat_id, $quantity)
    {
        $cat = Category::find($cat_id);
        $emps = Employee::all();
        //$work = productcustompillar::where('status','กำลังทำ')->where('end','>=',now())->orderBy('end', 'asc')->first();

        $work = productcustompillar::join('jobs', 'jobs.job_id', '=', 'productcustompillars.id')
            ->where('status', 'กำลังทำ')->where('end', '>=', now())->orderBy('end', 'asc')->get();
        $check = false;
        $num = 0;
        if (count($emps) > 0) {
            foreach ($emps as $emp) {
                if (count($work) > 0) {
                    foreach ($work as $job) {
                        if ($emp->id === $job->user_id) {
                            $num += 1;
                            $check = true;
                        }
                    }
                }
            }
        }
        //emp  = 1,2
        //work  = 1,2
        //num = 2

        if (count($work) <= 0) {

            //นับปกติ
            $day =  ceil($quantity / $cat->qtyperday) + 2;
        } else if ($check && ($num != count($emps))) {
            $day =  ceil($quantity / $cat->qtyperday) + 2;
        } else if (!empty($work)) {

            $date1 = now();
            $date2 = date_create($work[0]->end);
            $diff = date_diff($date1, $date2);
            $numdiff = $diff->format('%d') + 2;
            $day = ceil($quantity / $cat->qtyperday);

            $day += $numdiff;
        }

        if ($cat_id == 4) {
            if (count($work) <= 0) {

                //นับปกติ
                $day =  ceil($quantity / $cat->qtyperday) + 7;
            }

            if ($check && ($num != count($emps))) {

                $day =  ceil($quantity / $cat->qtyperday) + 7;
            } else if (!empty($work)) {

                $date1 = now();
                $date2 = date_create($work[0]->end);
                $diff = date_diff($date1, $date2);
                $numdiff = $diff->format('%d') + 7;
                $day = ceil($quantity / $cat->qtyperday);

                $day += $numdiff;
            }
        }

        return $day;
    }

    public function createInvoice()
    {
        return view('products.invoice')->with('categories', Category::all());
    }
}
