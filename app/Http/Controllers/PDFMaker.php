<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class PDFMaker extends Controller
{
    function gen()
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('orderitems.blade.php');
        return $pdf->stream();
    }
}
