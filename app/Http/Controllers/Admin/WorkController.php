<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\productcustompillar;
use App\Employee;
use App\Job;
use App\Category;


class WorkController extends Controller


{
    public function showWork(){
       
        //SELECT p.*, GROUP_CONCAT( u.name ) as my_users FROM productcustompillars p LEFT JOIN jobs j ON p.id = j.job_id LEFT JOIN users u ON j.user_id = u.id group by p.id
 
        $productcustompillars = DB::table('productcustompillars')
        ->select(DB::raw("productcustompillars.*, GROUP_CONCAT( employees.fname,' ',employees.lname ) as my_users"))
        ->leftjoin("jobs","productcustompillars.id","=","jobs.job_id")
        ->leftjoin("employees","jobs.user_id","=","employees.id")
        ->groupBy("productcustompillars.id")
        ->paginate(10);

        return view ('admin.Work',["productcustompillars"=>$productcustompillars]);
    
    }

    public function addWork($id){
        $users =  Employee::all();
        $productcustompillars = productcustompillar::find($id);
        return view('admin.addWork',compact('productcustompillars','users'));
    }

    public function editWork($id){
        $users =  Employee::all();
        $productcustompillars = productcustompillar::find($id);
        $jobs =  Job::where('job_id',$id)->get();

        $cat = Category::find($productcustompillars->category_id);
        // {{  !empty(date) ? fun(date) ? '' }}
       

        if($productcustompillars->category_id == 4){
            $day =  ceil(($productcustompillars->length*$productcustompillars->amount)/$cat->qtyperday)+7;
        }else{
            $day =  ceil(($productcustompillars->length*$productcustompillars->amount)/$cat->qtyperday);
        }
        $emplyeework = Job::select('*')
                 ->leftJoin('employees', 'employees.id', '=', 'jobs.user_id')
                 ->join('productcustompillars','productcustompillars.id','jobs.job_id')
                 ->whereIn('status',['กำลังทำ','เสร็จสิ้น'])
                 ->where('end','>=',now())
                 ->orderBy('end', 'desc')
                 //->groupBy("employees.id")
                 //->groupBy("jobs.user_id")
                 ->get();
        
        return view('admin.editWork',compact('productcustompillars','users','jobs','emplyeework','day'));
    }

    public function updateWord(Request $request){
       $word = productcustompillar::find($request->id);
       $word->start = $request->start;
       $word->end = $request->end;
       $word->status = $request->status;
    
       $word->cement_price = $request->cement_price;
       $word->soi_price = $request->soi_price;
       $word->iron_price = $request->iron_price;
       $word->work_price = $request->work_price;

       if($word->save()){
            $job_id =  $word->id;
            if(!empty($request->user)){
                foreach( $request->user as $user){
                    $job =  new Job;
                    $job->job_id = $job_id;
                    $job->user_id = $user;
                    $job->save();
                }
            }
       }
       return redirect()->route('showWork');
    }

    public function reworkWord(Request $request){
        $word = productcustompillar::find($request->id);
        $word->start = $request->start;
        $word->end = $request->end;
        $word->status = $request->status;
        $word->cement_price = $request->cement_price;
       $word->soi_price = $request->soi_price;
       $word->iron_price = $request->iron_price;
       $word->work_price = $request->work_price;
        if($word->save()){
            $job_id =  $word->id;
            Job::where('job_id',$request->id)->delete();
            if(!empty($request->user)){
                foreach( $request->user as $user){
                    $job =  new Job;
                    $job->job_id = $job_id;
                    $job->user_id = $user;
                    $job->save();
                }
            }
        }
        return redirect()->route('showWork');
    }
  

}
