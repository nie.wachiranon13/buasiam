<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SumController extends Controller
{
    public function showSum(){

        return view ('admin.Summary');
    
    }

    public function showSumPillars(){

        return view ('admin.SummaryPillars');
    
    }

    
        
}

