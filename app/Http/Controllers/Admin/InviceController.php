<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Invoice;
use Illuminate\Http\Request;
use PDF;

class InviceController extends Controller
{
    public function pdf($id)
    {
        $invoice = Invoice::where('id', $id)->where('status', '!=', 'รอการยืนยัน')->firstOrFail();
        $invoice_details =  $invoice->invoiceDetailAll()->get();


        // $total = $invoice_details->groupBy('id')->map(function ($row) {
        //     return $row->sum('total_price');
        // });

        $pdf = PDF::loadView('admin.pdf', [
            'invoice' =>  $invoice,
            'invoice_details' => $invoice_details,
            //'total' => $total->sum()
        ]);
        return  @$pdf->stream();
    }
}
