<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;

use App\Slip;


class OrderController extends Controller
{
    //
public function checkslip(){
    $slips = DB::table('slips')->paginate(10);
    
    return view ('admin.Checkslip',["slips"=>$slips]);
}

public function orderPanel(){

    $orders= DB::table('orders')->paginate(10);

    return view ('admin.OrderPanel',["orders"=>$orders]);

 


}
public function showOrderDetail($id){
   $orderitems= DB::table('orders')
    ->join('orderitems','orders.order_id','=','orderitems.order_id')
    ->where('orders.order_id',$id)
    ->get();
    return view('admin.orderDetails',["orderitems"=>$orderitems]);
    return view('admin.orderDetails',["orderitems"=>$orderitems]);

}

public function edit($id){
    $order = Order::find($id);
    $id = $id;
    return view('admin.ChangeStatus',compact('id'));
  

}
public function updateStatus(Request $request,$id){

    $order = Order::find($id);
 
    $order->status = $request->status;
 
    $order->save();
   
    Session()->flash("success","แก้ไชข้อมูลสำเร็จ!");
    return redirect('admin/orders');
 

            
}

}