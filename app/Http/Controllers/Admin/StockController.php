<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Stock;
use App\StockMaterial;
use App\Material;
use App\productcustompillar;
use Auth;


class StockController extends Controller
{


    public function showStock()
    {


        $products = DB::table('products')->paginate(10);

        return view('admin.Stock', ["products" => $products]);
    }

    public function addStock($id)
    {
        $product = Product::find($id);
        return view('admin.AddStock')->with('product', $product);
    }

    public function insertStockDetail(Request $request)
    {
        //หา product ที่เพิ่ม stock
        $product_use =  Product::find($request->product_id);


        //คำนวณ จำนวนวัตถุดิบที่ต้องใช้
        $cement = $product_use->cement *  $request->stock;
        $soi =  $product_use->soi *  $request->stock;
        $iron = $product_use->iron *  $request->stock;

        $material_cement = Material::find(1)->where('stock', '>=', $cement)->first();
        $material_soi = Material::find(2)->where('stock', '>=', $soi)->first();
        $material_iron = Material::find(3)->where('stock', '>=', $iron)->first();

        if ($material_cement && $material_soi && $material_iron) {
            //เช็ควัถุดิบพอหรือไหม
            $stock = (int)$request->stock;
            $check = $this->checkStock($cement, $soi, $iron, $request->product_id, $stock);

            if ($check) { //พอ  

                $product = Product::find($request->product_id)->increment('stock', $request->stock);

                return redirect()->route('product.index')->with('succuss', 'เพิ่มสต๊อกเรียบร้อย!!');
            } else { //ไม่พอ
                return  redirect()->route('addStockProduct', $request->product_id)->withErrors(['วัตถุดิบไม่เพียงพอ!!']);
            }
        } else {
            return  redirect()->route('addStockProduct', $request->product_id)->withErrors(['วัตถุดิบไม่เพียงพอ!!']);
        }
    }

    public function showDeatailStock($id)
    {
        $stocks = Product::where('products.id', $id)
            ->join('stocks', 'products.id', '=', 'stocks.product_id')
            ->orderByDesc('stocks.created_at')
            ->paginate(15);

        return view('admin.show-stock-detail', compact("stocks"));
    }

    public function checkStock($cement, $soi, $iron, $product_id, $stocks)
    {

        $product = Product::find($product_id);
        $stock =  new Stock;
        $stock->product_id = $product_id;
        $stock->quantity = $stocks;
        $stock->balance =  $stocks;
        $stock->save();

        $stock_id = $stock->lot_id;

        if ($cement) { //เช็คปูน
            $c_cement = $this->checkMaterial(1, $cement);
        }
        if ($soi) { //เช็คทราย
            $c_soi =  $this->checkMaterial(2, $soi);
        }
        if ($iron) { //เช็คเหล็ก
            $c_iron = $this->checkMaterial(3, $iron);
        }

        $s_cost = Stock::find($stock_id);
        $s_cost->cement_price = $c_cement;
        $s_cost->soi_price = $c_soi;
        $s_cost->iron_price = $c_iron;
        $s_cost->work_price = $stock->quantity * $product->workprice;

        $s_cost->save();


        $product = Product::find($product_id);

        //มอบหมายงาน
        $work =  new productcustompillar;
        $work->nameorder = $product->name;
        $work->name = Auth::user()->name;
        $work->tel = "-";
        $work->description = "-";
        $work->image = $product->image;
        $work->price = ($product->price  * $stocks);
        $work->length = $stocks;
        $work->width =  $product->width;
        $work->category_id =  $product->category_id;
        $work->amount =  "1";
        $work->cement_price = $c_cement;
        $work->soi_price = $c_soi;
        $work->iron_price = $c_iron;
        $work->work_price = $work->length * $product->workprice;
        $work->status = "รอการมอบหมายงาน";
        $work->mode = 'A';
        $work->save();


        return true;
    }

    public function checkMaterial($id, $quantity)
    {
        //เช็ค วัตถุดิบแต่ละ อัน
        $material = Material::find($id)->where('stock', '>=', $quantity)->firstOrFail();
        if ($material) {

            //เช็ค stock ว่ามีวัตถุดิบน้อยกว่า 100 หรือไหม
            $this->chechStock();

            //เช็ควัตถุดิบแต่ละชนิด ของ stock
            $stock = StockMaterial::where('material_id', $id)->where('status', 0)->firstOrFail();
            if (!empty($stock->balance)) {
                if (($stock->balance - $quantity) >= 0) { //ติดบวก (มีวัตถุดิบเพียงพอ)

                    StockMaterial::find($stock->id)->decrement('balance', $quantity);

                    if (($stock->balance - $quantity) == 0) {
                        //stock หมด
                        $save_stock  = StockMaterial::find($stock->id);
                        $save_stock->balance = 0;
                        $save_stock->status = 1;
                        $save_stock->save();
                    }

                    //ลด stock
                    Material::find($id)->decrement('stock', $quantity);


                    //คำนวณต้นทุน
                    return $cost = $stock->price * $quantity;
                } else { //ติดลบ  (มีวัตถุดิบไมเพียงพอ)
                    $item_l = ($stock->balance - $quantity) * -1; //-5

                    $save_stock  = StockMaterial::find($stock->id);

                    $cost =  $save_stock->quantity * $stock->price;

                    $save_stock->balance = 0;
                    $save_stock->status = 1;
                    $save_stock->save();

                    //ลด stock
                    Material::find($id)->decrement('stock', $stock->balance);

                    $this->checkMaterial($id, $item_l);
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public function chechStock()
    {
        $stocks = Material::all();
        foreach ($stocks as $stock) {
            if ($stock->stock < 100) {
                return abort(201);
            }
        }
        return true;
    }
}
