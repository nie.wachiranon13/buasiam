<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Invoice;
use App\ProductCustom;
use App\InvoiceDetail;

class OrdercustomController extends Controller
{
    //


    public function orderCustom()
    {

        $invoices = Invoice::paginate(10);

        return view('admin.OrderCustom', ["invoices" => $invoices]);
    }
    public function orderCustomDetail($id)
    {
        $invoice = Invoice::find($id);

        $invoice_details =  $invoice->invoiceDetailAll()->get();

        return view('admin.OrderCustomDetail', [
            "invoice" => $invoice,
            "invoice_details" => $invoice_details
        ]);
    }

    public function updateStatus(Request $request)
    {

        $invoice = Invoice::find($request->invoice_id);

        if ($request->status == 'ยืนยันการสั่งทำ' && $invoice->status == 'รอการยืนยัน') {
            //ตัดวัตถุดิบ
            $check_stock = new StockController();
            foreach ($request->productcustom_id as $key => $product_cus_id) {

                $product = ProductCustom::where('productcustom_id', $product_cus_id)->first();
                $cement =  $product->cement;
                $soi =  $product->soi;
                $iron =  $product->iron;

                if ($cement) { //เช็คปูน
                    $c_cement = $check_stock->checkMaterial(1, $cement);
                }
                if ($soi) { //เช็คทราย
                    $c_soi =  $check_stock->checkMaterial(2, $soi);
                }
                if ($iron) { //เช็คเหล็ก
                    $c_iron = $check_stock->checkMaterial(3, $iron);
                }

                $invoice_details = InvoiceDetail::find($request->id[$key]);
                $invoice_details->cost_cement = $c_cement ?? 0;
                $invoice_details->cost_soi = $c_soi ?? 0;
                $invoice_details->c_iron = $c_iron ?? 0;
                $invoice_details->cost = $c_cement + $c_soi + $c_iron + $product->mold_cost;
                $invoice_details->note = $request->note[$key];

                if (!empty($request->other_price[$key])) {
                    //update invoid
                    $invoice_details->other_price =  $request->other_price[$key];
                    $invoice->totalprice =   $invoice->totalprice + $request->other_price[$key];
                }
                $invoice_details->profit = ($invoice_details->total_price) - ($c_cement + $c_soi + $c_iron) + $invoice_details->other_price;
                $invoice_details->save();
            }
        }

        $invoice->status = $request->status;
        $invoice->save();

        return redirect()->route('orderCustom.index');
    }
}
