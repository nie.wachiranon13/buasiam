<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
  public function index(){

      return view('admin.CategoryForm')->with('categories',Category::paginate(7));
  }

  public function store(Request $request){
        $request->validate([
            'name' => 'required|unique:categories',
        ]);
        // เพิ่มข้อมูลเข้าไป
        $category=new Category;
        $category->name = $request->name;
        $category->qtyperday = $request->qtyperday;
        $category->save();

         Session()->flash("success","บันทึกข้อมูลสำเร็จ!");
        return redirect('/admin/createCategory');
  }

  public function edit($id){
        $category=Category::find($id);
        return view('admin.EditCategoryForm',['category'=>$category]);
  }
  public function update(Request $request,$id){
        $request->validate([
            'name' => 'required|unique:categories',
        ]);
        $category=Category::find($id);
        $category->name=$request->name;
        $category->qtyperday = $request->qtyperday;
        $category->save();
        Session()->flash("success","แก้ไขข้อมูลสำเร็จ!");
        return redirect('/admin/createCategory');
  }
  
  public function delete($id){

      $category=Category::find($id);
        if($category->products->count()>0){
            Session()->flash("warning","ไม่สามารถลบหมวดหมู่ได้ เนื่องจากมีสินค้าอยู่ในหมวดหมู่");
              return redirect()->back();
        }

        Category::destroy($id);
       
        Session()->flash("success","ลบข้อมูลสำเร็จ!");
        return redirect('/admin/createCategory');
  }

}
