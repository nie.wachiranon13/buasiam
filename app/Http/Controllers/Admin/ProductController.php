<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Controllers\UploadController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Product;
use App\ProductCustom;
use App\Stock;


class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware("verifyIsCategory")->only(['create', 'store']);
    }

    public function index()
    {

        //โยนข้อมูลไปdashboard
        return view('admin.ProductDashboard')->with('products', Product::paginate(3));
    }
    public function create()
    {
        return view('admin.productForm')->with('categories', Category::all());
    }
    public function createCustom()
    {
        return view('admin.productCustomForm')->with('categories', Category::all());
    }

    public function edit($id)
    {

        $product = Product::find($id);
        return view('admin.editProductForm')->with('product', $product)
            ->with('categories', Category::all());
    }

    public function editImage($id)
    {

        $product = Product::find($id);
        return view('admin.editProductImage')->with('product', $product);
    }
    public function updateImage(Request $request, $id)
    {

        $request->validate([


            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',


        ]);

        if ($request->hasFile("image")) {
            $product = Product::find($id);
            $exists = storage::disk('local')->exists("public/product_image/" . $product->image);

            if ($exists) {
                Storage::delete("public/product_image/" . $product->image);
            }
            $request->image->storeAs("public/product_image/", $product->image);
            Session()->flash("success", "แก้ไขรูปภาพสำเร็จ!");
            return \redirect('/admin/dashboard');
        }
    }

    public function update(Request $request, $id)

    {
        $request->validate([
            'name' => 'required',

            // 'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',
            'description' => 'required',

            'category' => 'required',


        ]);

        $upload =  new UploadController();

        $product = Product::find($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->stock = $request->stock;

        if ($request->category) {

            $product->category_id = $request->category;
        }

        if (!empty($request->image)) {
            $product->image  = $upload->uploadeFileProduct($request->image);
        }

        $product->save();
        Session()->flash("success", "แก้ไชข้อมูลสำเร็จ!");
        return redirect('/admin/dashboard');
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $exists = storage::disk('local')->exists("public/product_image/" . $product->image);

        if ($exists) {
            Storage::delete("public/product_image/" . $product->image);
        }
        Product::destroy($id);

        Session()->flash("success", "ลบข้อมูลสำเร็จ!");
        return redirect('/admin/dashboard');
    }
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'description' => 'required',
            'category' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'image' => 'required|file|image|mimes:jpeg,png,jpg|max:5000',
            'width' => 'required|numeric',
            'cement' => 'required|numeric',
            'soi' => 'required|numeric',
            'iron' => 'required|numeric',
            'workprice' => 'required|numeric',

        ]);

        $upload = new UploadController();
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->category_id = $request->category;
        $product->price = $request->price;
        $product->width = $request->width;
        $product->stock = $request->stock;
        $product->image  = $upload->uploadeFileProduct($request->image);
        $product->cement = $request->cement;
        $product->soi = $request->soi;
        $product->iron = $request->iron;
        $product->workprice = $request->workprice;
        $product->save();

        // if($product->save()){
        //     $stock =  new Stock;
        //     $stock->product_id =  $product->id;
        //     $stock->cement_price =  $request->cement_price;
        //     $stock->soi_price =  $request->soi_price;
        //     $stock->iron_price =  $request->price;
        //     $stock->quantity =  $request->stock;
        //     $stock->save();
        // }

        Session()->flash("success", "บันทึกข้อมูลสำเร็จ!");
        return redirect('/admin/dashboard');
    }
    public function storeCustom(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'description' => 'required',
            'category' => 'required',
            'price' => 'required|numeric',


            'width' => 'required|numeric',


            'cement' => 'required|numeric',
            'soi' => 'required|numeric',
            'iron' => 'required|numeric',
            'workprice' => 'required|numeric',

        ]);

        // $stringImageReFormat=base64_encode('_'.time());
        // $ext=$request->file('image')->getClientOriginalExtension();
        // $imageName=$stringImageReFormat.".".$ext;
        // $imageEncoded=File::get($request->image);

        // Storage::disk('local')-> put('public/product_image/'.$imageName,$imageEncoded); //กำหนดค่าปลายทางที่เก็บรูปภาพ
        $product = new ProductCustom;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->category_id = $request->category;
        $product->price = $request->price;
        $product->width = $request->width;
        $product->length = $request->length;
        $product->heigth = $request->heigth;



        $product->cement = $request->cement;
        $product->soi = $request->soi;
        $product->iron = $request->iron;
        $product->workprice = $request->workprice;

        $product->mold = $request->mold;
        $product->save();

        // if($product->save()){
        //     $stock =  new Stock;
        //     $stock->product_id =  $product->id;
        //     $stock->cement_price =  $request->cement_price;
        //     $stock->soi_price =  $request->soi_price;
        //     $stock->iron_price =  $request->price;
        //     $stock->quantity =  $request->stock;
        //     $stock->save();
        // }

        Session()->flash("success", "บันทึกข้อมูลสำเร็จ!");
        return redirect('/admin/dashboard');
    }
    public function orderCustom()
    {

        //โยนข้อมูลไปdashboard
        return view('admin.orderCustom');
    }
}
