<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    public function show(){

        return view('admin.ManageEmployee')->with('employees',Employee::paginate(7));
    }
    public function create(){
        return view('admin.EmployeeForm');

        
    }

    public function store(Request $request){
        $request->validate([
            
            'fname'=>'required',
            'lname'=>'required',
            'address'=>'required',
            'position'=>'required',
            'idcard'=>'required',
           
            

        
            ]);
        
            $employee = new Employee;
            $employee->fname =$request->fname;
            $employee->lname = $request->lname;
            $employee->idcard = $request->idcard;
            $employee->address = $request->address;
            $employee->position=$request->position;
        
            $employee->save();

         
            Session()->flash("success","บันทึกข้อมูลสำเร็จ!");
            return redirect('/admin/Employee');
            
  
            }
            public function update(Request $request,$id)
    
            {
                $request->validate([
                'fname'=>'required',
                'lname'=>'required',
                
                'idcard'=>'required',
                'position'=>'required',
                
    
                ]);
                    
                $employee = Employee::find($id);
                $employee->fname =$request->fname;
                $employee->lname = $request->lname;
                $employee->idcard = $request->idcard;
                $employee->address = $request->address;
                $employee->position=$request->position;
               
               
               
    
                $employee->save();
                Session()->flash("success","แก้ไชข้อมูลสำเร็จ!");
                return redirect('/admin/Employee');
                
    
                
            }
            public function delete($id){
                $employee=Employee::find($id);
                
    
                Employee::destroy($id);
    
                Session()->flash("success","ลบข้อมูลสำเร็จ!");
                return redirect('/admin/dashboard');
    
            }
            public function edit($id){

                $employee = Employee::find($id);
                $id = $id;
                return view('admin.editEmpForm',compact('id'));
        
        
        
            }
}
