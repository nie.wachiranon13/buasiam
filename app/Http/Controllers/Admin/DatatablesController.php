<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Order;
use App\Orderitem;
use DB;
use App\productcustompillar;

class DatatablesController extends Controller
{
    public function orderData(Request $request)
    {
        //select `orders`.* ,SUM(orderitems.cost) from `orders` LEFT join orderitems ON (orders.order_id = orderitems.order_id) GROUP BY orders.order_id
        $oders = Order::join('orderitems', 'orders.order_id', '=', 'orderitems.order_id')
        ->select(DB::raw("orders.created_at as created_at,concat(orders.fname, ' ', orders.lname) as order_name ,SUM(orderitems.cost) as cost,orders.price as sell,orders.price -SUM(orderitems.cost) as total"));
        if(empty( $request->filter_mouth)){
            $oders->whereMonth('orders.created_at',date('m'));
        }else{
            $oders->whereMonth('orders.created_at',$request->filter_mouth+1);
        }
        if(empty( $request->filter_year)){
           $oders->whereYear('orders.created_at',date('Y'));
        }else{
            $oders->whereYear('orders.created_at',$request->filter_year);
        }
        $oders->where('status','ชำระเงินแล้ว');
        $oders->groupBy('orders.order_id')
        ->get();

      
        $data = $oders ?? [];

        return Datatables::of($data)->make(true);
    }

    public function total(Request $request)
    {
        $oders = Order::join('orderitems', 'orders.order_id', '=', 'orderitems.order_id')
        ->select(DB::raw("orders.price -SUM(orderitems.cost) as total"));
        if(empty($request->filter_mouth)){
            $oders->whereMonth('orders.created_at',date('m'));
        }else{
            $oders->whereMonth('orders.created_at',$request->filter_mouth+1);
        }
        if(empty($request->filter_year)){
           $oders->whereYear('orders.created_at',date('Y'));
        }else{
            $oders->whereYear('orders.created_at',$request->filter_year);
        }
        $oders->where('status','ชำระเงินแล้ว');
        $oders->groupBy('orders.order_id');
    
      
        $sum_total = 0;
        foreach($oders->get() as $item){
        
            $sum_total = $sum_total +  $item->total;
        }
        

        return  json_encode(["total" => $sum_total]);
    }

    public function orderDataPillars(Request $request)
    {
        //select `orders`.* ,SUM(orderitems.cost) from `orders` LEFT join orderitems ON (orders.order_id = orderitems.order_id) GROUP BY orders.order_id
        $oders = productcustompillar::
        select(DB::raw("created_at as created_at,name as order_name ,
        SUM(soi_price + iron_price + cement_price + work_price) as cost,
        price as sell, price - SUM(soi_price + iron_price + cement_price +work_price) as total"));
        if(empty( $request->filter_mouth)){
            $oders->whereMonth('created_at',date('m'));
        }else{
            $oders->whereMonth('created_at',$request->filter_mouth+1);
        }
        if(empty( $request->filter_year)){
           $oders->whereYear('created_at',date('Y'));
        }else{
            $oders->whereYear('created_at',$request->filter_year);
        }
        $oders->where('status','กำลังทำ');
        $oders->where('mode','C')
        ->get();

      
        $data = $oders ?? [];

        return Datatables::of($data)->make(true);
    }

    public function totalPillars(Request $request)
    {
        $oders = productcustompillar::
        select(DB::raw("created_at as created_at,name as order_name ,
        SUM(soi_price + iron_price + cement_price + work_price) as cost,
        price as sell, price - SUM(soi_price + iron_price + cement_price +work_price) as total"));
        if(empty( $request->filter_mouth)){
            $oders->whereMonth('created_at',date('m'));
        }else{
            $oders->whereMonth('created_at',$request->filter_mouth+1);
        }
        if(empty( $request->filter_year)){
           $oders->whereYear('created_at',date('Y'));
        }else{
            $oders->whereYear('created_at',$request->filter_year);
        }
        $oders->whereIn('status',['กำลังทำ','เสร็จสิ้น']);
       
      
        $oders->where('mode','C');
    
      
        $sum_total = 0;
        foreach($oders->get() as $item){
        
            $sum_total = $sum_total +  $item->total;
        }
        

        return  json_encode(["total" => $sum_total]);
    }
    
}
