<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Material;
use App\StockMaterial;
class MaterialController extends Controller
{
    public function index(){

    
        return view('admin.material')->with('materials',Material::paginate(3));

        
    }
    public function showStock(){

      
        $materials= DB::table('materials')->paginate(10);

    return view ('admin.stockMaterial',["materials"=>$materials]);

    }


    public function create(){
        return view('admin.MaterialForm');

        
    }
    public function store(Request $request){
        $request->validate([
            
            'name'=>'required',
          
            ]);
        
            $material = new Material;
            $material->name =$request->name;
            $material->stock =$request->stock;
            $material->save();

            
            Session()->flash("success","บันทึกข้อมูลสำเร็จ!");
            return redirect('/admin/material');
            
  
            }

           

            public function addStock($id){
                $material = Material::find($id);
                return view('admin.AddstockMaterial')->with('material',$material);
        
               
        
            }
            public function insertStockDetail(Request $request){
        
                $stockM =  new StockMaterial;
                $stockM->material_id =  $request->material_id;
            
                $stockM->price =  $request->price;
                $stockM->quantity =  $request->quantity;
                $stockM->status =  0;
                $stockM->balance =  $request->quantity;
                
                if($stockM->save()){
                    $material = Material::find($request->material_id)->increment('stock',$request->quantity);
                }
                
                return redirect()->route('material.index')->with('succuss','เพิ่มสต๊อกเรียบร้อย!!');
        
            }

            public function showDeatailStock($id){
                $stock_materials = Material::where('materials.id',$id)
                ->join('stock_materials', 'materials.id', '=', 'stock_materials.material_id')
                ->orderByDesc('stock_materials.created_at')
                ->paginate(15);
          
                return view('admin.show-stock-material',compact("stock_materials"));
            }
}
