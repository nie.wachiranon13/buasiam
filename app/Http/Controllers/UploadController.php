<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UploadController extends Controller
{
    public function uploadeImage($request)
    {
        $stringImageReFormat = Str::random(10);
        $ext = $request->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request);

        Storage::disk('local')->put('public/product_custom/' . $imageName, $imageEncoded); //กำหนดค่าปลายทางที่เก็บรูปภาพ
        return 'product_custom/' . $imageName;
    }

    public function uploadeFile($request, $folder = '')
    {
        if (!empty($folder)) {
            $folder =  $folder . '/';
        } else {
            $folder = '';
        }
        $stringImageReFormat = Str::random(10);
        $ext = $request->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request);

        Storage::disk('local')->put('public/' . $folder . $imageName, $imageEncoded); //กำหนดค่าปลายทางที่เก็บรูปภาพ

        return $folder .  $imageName;
    }

    public function uploadeFileProduct($request, $folder = '')
    {
        if (!empty($folder)) {
            $folder =  $folder . '/';
        } else {
            $folder = '';
        }
        $stringImageReFormat = Str::random(10);
        $ext = $request->getClientOriginalExtension();
        $imageName = $stringImageReFormat . "." . $ext;
        $imageEncoded = File::get($request);

        Storage::disk('local')->put('public/product_image/' . $folder . $imageName, $imageEncoded); //กำหนดค่าปลายทางที่เก็บรูปภาพ

        return $folder .  $imageName;
    }
}
