<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Category;

use App\Orderitem;

class PrintController extends Controller
{
    public function index()
    {
          $orderitems = Orderitem::all();
          return view('products.printOrder')->with('orderitems', $orderitems)
          ->with("categories",Category::all()->sortBy('name'));;
    }
    public function prnpriview()
    {
          $orderitems = Orderitem::all();
          return view('products.orderitems')->with('orderitems', $orderitems)
          ->with("categories",Category::all()->sortBy('name'));;
    }
}
