<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $fillable = [
        'productcustom_id',
        'image',
        'amount',
        'detail',
        'total_price'
    ];

    public function invoice()
    {
        return $this->belongsTo(invoice::class);
    }

    public function product()
    {
        return $this->belongsTo(ProductCustom::class, 'productcustom_id', 'productcustom_id');
    }
}
