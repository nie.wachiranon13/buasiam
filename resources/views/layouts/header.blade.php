<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>หน้าแรก | บัวสยาม</title>
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">

	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('css/main.css')}}" rel="stylesheet">
	<link href="{{asset('css/responsive.css')}}" rel="stylesheet">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">


</head>
<!--/head-->

<body>
	<header id="header">
		<div class="header_top">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--/header_top -->

		<div class="header-middle">
			<!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="/products"><img src="{{asset('images/home/buasiamm.png')}}" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">

								@if(Auth::check())

								<li><a href="/products/cart">
										@if(isset($cartItems))
										<span class="badge" style="background:red">{{$cartItems->totalQuantity}}</span>
										@endif
										<i class="fa fa-shopping-cart"></i> รถเข็น</a></li>
								<li>

									<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
										<i class="fa fa-sign-out-alt"></i>{{ __('ออกจากระบบ') }}
									</a>


									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										@csrf
									</form>
								</li>
								@else


								<li><a href="/login"><i class="fa fa-lock"></i> ลงชื่อเข้าใช้</a></li>
								<li><a href="/register"><i class="fa fa-user"></i> สมัครสมาชิก</a></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-middle-->

		<div class="header-bottom">
			<!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="/products" class="active">หน้าแรก</a></li>
								<!-- <li class="dropdown"><a href="#">ร้านค้า<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">สินค้า</a></li>
										<li><a href="product-details.html">รายละเอียดสินค้า</a></li>
										<li><a href="">ลงชื่อออก</a></li>
										<li><a href="cart.html">รถเข็น</a></li>
										<li><a href="login.html">ลงชื่อเข้าใช้</a></li>
                                    </ul>
                                </li> -->

								<li><a href="/products/createCustom">สั่งสินค้า</a></li>
								<li><a href="{{route('showslip')}}">แจ้งชำระเงิน</a></li>
								<li><a href="">ตรวจสอบสถานะการชำระสินค้า</a></li>
								<li><a href="/products/checkstatuspayment">ประวัติการสั่งซื้อ</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<form class="" action="/products/search" method="get">
								<input type="text" placeholder="ค้นหาสินค้า" name="search" />

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-bottom-->
	</header>
	<!--/header-->
	<br>
	<br>
	<br>
	<section id="slider">
		<!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>

						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span></span>บัวสยาม</h1>
									<h2> ขาย ผลิต ติดตั้งบัวปูนปั้น เสาโรมัน </h2>
									<p> </p>

								</div>
								<div class="col-sm-6">
									<img src="{{asset('images/home/eiei.jpg')}}" class="girl img-responsive" alt="" width="9600px" height="540px" />
									<!-- <img src="images/home/pricing.png"  class="pricing" alt="" /> -->
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span>ร</span>บัวสยาม</h1>
									<h2>บริการ ทุกระดับประทับใจ</h2>


								</div>
								<div class="col-sm-6">
									<img src="{{asset('images/home/sing.jpg')}}" class="girl img-responsive" alt="" width="9600px" height="540px" />

								</div>
							</div>

							<!-- <div class="item">
								<div class="col-sm-6">
									<h1><span>E</span></h1>
									<h2>3 ปี</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="images/home/girl3.jpg" class="girl img-responsive" alt="" />
									<img src="images/home/pricing.png" class="pricing" alt="" />
								</div>
							</div> -->

						</div>

						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!--/slider-->