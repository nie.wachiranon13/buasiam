<footer id="footer">
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					
					<!-- <div class="col-sm-2">
						<div class="single-widget">
							<h2>Quock Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">T-Shirt</a></li>
								<li><a href="#">Mens</a></li>
								<li><a href="#">Womens</a></li>
								<li><a href="#">Gift Cards</a></li>
								<li><a href="#">Shoes</a></li>
							</ul>
						</div>
					</div> -->
					

					<!-- @foreach($categories as $category)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="/products/category/{{$category->id}}">{{($category->name)}}</a></h4>
								</div>
							</div>
						
						@endforeach	 -->
						
					<div class="col-sm-6">
				
						<div class="single-widget">
						
							<h2>หมวดหมู่สินค้า</h2>
							<ul class="nav nav-pills nav-stacked">
							@foreach($categories as $category)
							<h2 class="single-widget">
							
							<li><a href="/products/category/{{$category->id}}">{{($category->name)}}</a></li>
							@endforeach	
							</ui>
							</h2>
						</div>
					
					</div>
					<div class="col-sm-6">
						<div class="single-widget">
							<h2>สั่งทำสินค้า</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="/products/pillar">เสาโรมัน</a></li>
								<li><a href="/products/cutebua">คิวบัว</a></li>
								<li><a href="/products/headpillar">หัวเสา</a></li>
								<li><a href="/products/create3d">ลวดลาย</a></li>
								
							</ul>
						</div>
					</div>
					

				</div>
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Buasiam</p>
				</div>
			</div>
		</div>
	</footer>

  <script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('js/price-range.js')}}"></script>
  <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>
  <script src="{{asset('js/main.js')}}"></script>

  
</body>
</html>