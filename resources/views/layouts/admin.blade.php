<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>ผู้ดูแลระบบ</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="{{asset('css/sidebar.css')}}" rel="stylesheet">
  <link href="{{asset('css/app.css')}}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
</head>

<body>
  <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">ส่วนของผู้ดูแลระบบ</h5>
    <nav class="my-2 my-md-0 mr-md-3">

      <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
        {{ __('ออกจากระบบ') }}
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>


      @php

      $materials = App\Material::all();
      $stock = true;
      foreach($materials as $material){
      if($material->stock < 100){ $stock=false; break; } } @endphp </nav>
  </div>
  <div class="d-flex" id="wrapper">
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">เมนู</div>
      <div class="list-group list-group-flush">
        <a href="/admin/dashboard" class="list-group-item list-group-item-action bg-light">รายการสินค้า</a>
        <a href="/admin/createProduct" class="list-group-item list-group-item-action bg-light">เพิ่มรายการสินค้า</a>
        <a href="/admin/createCustom" class="list-group-item list-group-item-action bg-light">เพิ่มรายการสินค้าสั่งทำ</a>
        <a href="/admin/createCategory" class="list-group-item list-group-item-action bg-light">หมวดหมู่</a>
        <!-- <a href="/admin/stock" class="list-group-item list-group-item-action bg-light">สั่งเพิ่มสต๊อกสินค้า</a> -->
        <a href="/admin/checkslip" class="list-group-item list-group-item-action bg-light">รายการแจ้งชำระสินค้า</a>
        <a href="/admin/orders" class="list-group-item list-group-item-action bg-light">รายการสั่งซื้อ</a>
        <a href="/admin/orderCustom" class="list-group-item list-group-item-action bg-light">รายการสั่งทำ</a>
        <a href="{{ route('showWork') }}" class="list-group-item list-group-item-action bg-light">ตารางงาน</a>
        <a href="/admin/material" class="list-group-item list-group-item-action bg-light">วัตถุดิบ @if( !$stock )<span class="
                                badge badge-danger
                        ">วัตถุดิบเหลือน้อย</span>@endif</a>
        <!-- <a href="/admin/MaterialForm" class="list-group-item list-group-item-action bg-light">เพิ่มรายการวัตถุดิบ</a> -->
        <!-- <a href="/admin/stockMaterial" class="list-group-item list-group-item-action bg-light">เพิ่มสต๊อกวัตถุดิบ</a> -->
        <a href="/admin/Employee" class="list-group-item list-group-item-action bg-light">พนักงาน</a>
        <a href="{{route('showSum')}}" class="list-group-item list-group-item-action bg-light">รายได้จากรายการสั่งซื้อ</a>
        <a href="{{route('showSumPillars')}}" class="list-group-item list-group-item-action bg-light">รายได้จากรายการสั่งทำ</a>
      </div>
    </div>
    <div id="page-content-wrapper">
      <div class="container-fluid">
        @if(Session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{Session()->get('success')}}
        </div>
        @endif

        @if(Session()->has('warning'))
        <div class="alert alert-danger" role="alert">
          {{Session()->get('warning')}}
        </div>
        @endif

        @yield('body')
      </div>
    </div>
</body>

</html>