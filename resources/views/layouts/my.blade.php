<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css">
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>หน้าแรก | บัวสยาม</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">

    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
	  <link href="{{asset('css/main.css')}}" rel="stylesheet">
	  <link href="{{asset('css/responsive.css')}}" rel="stylesheet">

<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/jquery.printPage.js"></script>
</head>

<body>
<div class="container">
<div class="col-md-12">

@yield('content')
</div>
</div>
</body>
</html>