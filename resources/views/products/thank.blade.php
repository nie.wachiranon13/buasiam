@extends("layouts.index")
@section("content")

<figure class="text-center"> 
   <figcaption class="figure-caption"></figcaption>
  <img src="{{asset('images/home/บัวสยาม.jpg')}}" class="figure-img img-fluid rounded" alt="...">
  @if (session('diff_date'))
    <div class="alert alert-success">
        {{ "ลูกค้ารอสินค้าประมาณ".session('diff_date')." วัน" }}
    </div>
@endif
</figure> 
@endsection