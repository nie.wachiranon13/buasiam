@extends("layouts.index")

@section("content")


<!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>หน้าแรก | บัวสยาม</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="left-sidebar">

                </div>
            </div>
        </div>



        <div class="table-responsive mb-4">
            <h2>รายการสั่งซื้อ</h2>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">รหัสสั่งซื้อ</th>
                        <th scope="col">วันที่สั่งซื้อ</th>
                        <th scope="col">ขื่อผู้สั่ง</th>

                        <th scope="col">ราคา</th>


                        <th scope="col">สถานะ</th>
                        <th scope="col">รายละเอียดใบสั่งซื้อ</th>


                    </tr>
                </thead>
                <tbody>

                    @foreach($orders as $order)

                    <tr>
                        <td scope="row">{{$order->order_id}}</td>
                        <td scope="row">{{DateThai($order->created_at)}}</td>
                        <td scope="row">{{$order->fname}}</td>
                        <td scope="row">{{number_format($order->price)}}</td>
                        <td scope="row">{{$order->status}}</td>

                        <td>
                            <a href="{{route('orderdetailforauth',$order->order_id)}}" class="btn btn-info">รายละเอียด</a>
                        </td>



                        <!-- <td>
                        <a href=" " class=" btn btn-info">สั่งพิมพ์</a>
                    </td> -->


                    </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
        <div class="table-responsive mb-4">
            <h2>รายการสั่งทำ</h2>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">รหัสสั่งทำ</th>
                        <th scope="col">วันที่สั่งทำ</th>
                        <th scope="col">ขื่อผู้สั่ง</th>
                        <th scope="col">ราคา</th>


                        <th scope="col">สถานะ</th>
                        <th scope="col">รายละเอียดใบสั่งซื้อ</th>



                    </tr>
                </thead>
                <tbody>

                    @foreach($invoices as $invoice)

                    <tr>
                        <td scope="row">{{$invoice->id }}</td>
                        <td scope="row">{{DateThai($invoice->created_at)}}</td>
                        <td scope="row">{{$invoice->cus_name}}</td>
                        <td scope="row">{{number_format($invoice->totalprice)}}</td>
                        <td scope="row">{{$invoice->status }}</td>


                        <td>
                            <a href="" class="btn btn-info">รายละเอียด</a>
                        </td>



                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>



</body>

</html>
@endsection