@extends("layouts.index")
@extends("layouts.custom")
@section("content")
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  
<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="left-sidebar">
						
    
					
						</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
<div class="table-responsive">
    <h2>คิ้วบัว</h2>
    <form action="storeCutebua" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
         <div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  คิ้วบัว 
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

  <a class="dropdown-item" href="/products/pillar">เสาโรมัน</a>
    <a class="dropdown-item" href="/products/headpillar">หัวเสา</a>
    <a class="dropdown-item" href="/products/create3d">ลวดลายสามมิติ</a>
    
   
  </div>
</div>
<br>
<br>

 <div class="form-group">
    <label for="type">ชื่อสินค้า</label>
            <input type="text" class="form-control" name="nameorder" id="nameorder" placeholder="ชื่อ" required>
        </div>

    
       
        <div class="form-group">
            <label for="image">รูป</label>
            <input type="file" class="form-control"  name="image" id="image" required>
        </div> 

        <div class="form-group">
            <label for="type">ขนาดความกว้างคิ้วบัว(เซนติเมตร)</label>
            <input type="text" class="form-control" name="width" id="width" placeholder="เซนติเมตร" required>
            
            
        
        </div>
        <div class="form-group">
            <label for="type">ความยาว(เมตร)</label>
            <input type="text" class="form-control" name="length" id="length" placeholder="เมตร" required>
            
        <div class="form-group">
            <label for="type">จำนวนที่สั่ง(ต้น)</label>
            <input type="text" class="form-control" name="amount" id="amount" required>
        </div>

    
      
  <div class="form-group">
            <label for="type">รายละอียดสินค้าเพิ่มเติม</label>
            <input type="text" class="form-control" name="description" id="description" placeholder="รายละเอียด" >
        </div>
        
        <label for="type">ชื่อผู้สั่ง</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อ" required>
        </div>
        <label for="type">หมายเลขโทรศัพทฺ์</label>
            <input type="text" class="form-control" name="tel" id="tel" placeholder="หมายเลขติดต่อ" required>
        </div>

        <input type="hidden" name="status" value="รอการมอบหมายงาน">


        
        <div class="form-group">

        <input  type="hidden" name="price" id="price_hidden">
            <label for="type">ประเมิณประเมิณราคา(คลิ๊ก "คำนวณราคา" เพื่อคำนวณอัตโนมัติ)</label>
            <input type="text" class="form-control"  disabled  id="price">
        </div>
      
        
        <section id="do_action">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="total_area">
						<ul>
							<li>ราคาแบบพิมพ์ <span id = "pim"></span></li>
							
							<!-- <li>ค่าส่งสินค้า <span>Free</span></li> -->
							<li>ราคารวม สินค้าที่ต้องชำระทั้งหมด
                            
                             <span id ="price2">
                             
                            </span></li>
						</ul>
                        <input class="btn btn-primary" type="button" onClick="calc()" value="คำนวณราคา" />
                        
							<!-- <button class="btn btn-default check_out" type="submit" name="submit">สั่งสินค้า</a> -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  
                            สั่งสินค้า
                            </button>          

                        <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">คุณตกลงที่จะสั่งสินค้า</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
          <p>ตรวจสอบข้อมูลที่สั่งว่าถูกต้อง สมบูรณ์ "ตกลง"</p>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
        <button type="submit" class="btn btn-secondary">ตกลง</button>
      </div>
    </div>
  </div>
</div>

					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
    </form>
    
    <!doctype html>
<html>
<head>
<meta charset="utf-8">


<script>

function calc(){
var width=document.getElementById('width').value;
var length=document.getElementById('length').value;
var amount=document.getElementById('amount').value;

var sum=((length*10)*width*amount);
var pim = 800;

var sumprice = sum+pim;
console.log(sumprice);
document.getElementById('amount').innerHTML=amount;

document.getElementById('price2').innerHTML=sumprice;
document.getElementById('pim').innerHTML=pim;
document.getElementById("price").value = sumprice;
document.getElementById("price_hidden").value = sumprice;
//document.getElementById("price").disabled = true;
return false

}
</script>
</head>
<body>
<form>

</form>
<div id="price"></div>
</body>
</html>

    
    
        <!-- <button type="submit" name="submit" class="btn btn-success">ยืนยัน</button> -->
    
        
    <br>
  
</div>






    
   <figure class="text-center"> 
   <figcaption class="figure-caption">รูปแบบตัวอย่าง</figcaption>
  <img src="{{asset('images/home/cutebuanew.jpg')}}" class="figure-img img-fluid rounded" alt="...">
 
</figure> 
@endsection