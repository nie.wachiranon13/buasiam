@extends("layouts.index")

@section("content")


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>หน้าแรก | บัวสยาม</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

</head>
<!--/head-->

<body>
    <div class="container">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

    </div>
    <br>
    <br>
    <div class="container">

        <h2>เลือกสินค้าที่ต้องการสั่งทำ</h2>

        <br>
        @if (Session::has('error'))
        <div class="alert alert-danger">
            <ul>
                <li>{{ Session::get('error') }}</li>

            </ul>
        </div>
        @endif

        <div id="master" class="hidden">
            <div class="box-clone">
                <div class="form-group">
                    <label for="exampleInputFile">เลือกสินค้า</label>
                    <select class="form-control" name="productcustom_id[]" required>
                        <option>--เลือกสินค้า--</option>
                        @foreach ($product_customs as $product)
                        <option value="{{  $product->productcustom_id }}">{{ $product->name }}</option>
                        @endforeach
                    </select>

                </div>

                <div class="form-group">
                    <label for="exampleInputFile">ไฟล์รูปภาพแบบที่ต้องการ</label>
                    <input type="file" name="product_custom_image[]">

                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">จำนวนสินค้า</label>
                    <input type="number" class="form-control" min="0" name="quantity[]" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">รายละเอียดสินค้า</label>
                    <textarea class="form-control" name="detail[]" rows="3" placeholder="รายละเอียดสินค้า"></textarea>
                </div>
                <button type="button" onclick="removeEl(this)" class="btn btn-danger mb-4 flot-left remove">ลบ</button>
                <hr>
            </div>
        </div>

        <form action="{{ route('custom_order.store') }}" method="post" enctype="multipart/form-data">
            @csrf


            <div class="box-clone-to mb-4"></div>

            <button type="button" id="eventBtn" class="btn btn-info mb-4">เพิ่มรายการสินค้าสั่งทำ</button>

            <hr>

            <div class="form-group">
                <label for="exampleInputName">ชื่อผู้สั้งสินค้า</label>
                <input type="name" class="form-control" name="cus_name" placeholder="" required>
            </div>
            <div class="form-group">
                <label for="exampleInputTel">เบอร์โทรศัพท์ที่สามารถติดต่อได้</label>
                <input type="tel" class="form-control" name="cus_tel" placeholder="" required>
            </div>
            <br>

            <button type="submit" class="btn btn-primary">สั่งทำสินค้า</button>

        </form>
        <br>
        <br>
    </div>

    <script>
        $(document).ready(function() {

            var cloneCount = 0;
            $("#eventBtn").click(function() {
                $("#master > .box-clone")
                    .clone()
                    .attr('id', 'clone-box-' + cloneCount++)
                    .appendTo(".box-clone-to");
            });

        });

        function removeEl(selector) {
            console.log($(selector).parent().remove())
        }
    </script>
</body>

@endsection