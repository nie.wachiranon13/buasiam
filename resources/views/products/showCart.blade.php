@extends("layouts.index")
@section("content")
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="/products">หน้าแรก</a></li>
				  <li class="active">ตะกร้าสินค้า</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">สินค้า</td>
							<td class="description">รายละเอียด</td>
							<td class="price">ราคา</td>
							<!-- <td class="length">ความยาว(เมตร)</td> -->
                            <td class="quantity">จำนวนที่สั่งซื้อ(รายการ)</td>
                            
							<td class="total">ราคารวม</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					@foreach($cartItems->items as $item)
                    <tr>
							<td class="cart_product">
								<a href="/products/details/{{$item['data']['id']}}"><img src="{{asset('storage')}}/product_image/{{$item['data']['image']}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="/products/details/{{$item['data']['id']}}">{{$item['data']['name']}}</a></h4>
								<p>{{$item['data']['description']}}</p>
							</td>
							<td class="cart_price">
								<p>{{number_format($item['data']['price'],2)}}</p>
							</td>
                            

							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href="/products/cart/incrementCart/{{$item['data']['id']}}"> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$item['quantity']}}" autocomplete="off" size="2">
									<a class="cart_quantity_down" href="/products/cart/decrementCart/{{$item['data']['id']}}"> - </a>
								</div>
							</td>
                            
							
							<td class="cart_total">
								<p class="cart_total_price">{{$item['totalSinglePrice']}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่?')"href="/products/cart/deleteFromCart/{{$item['data']['id']}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>

                    @endforeach


					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="total_area">
						<ul>
							<li>จำนวนสินค้า <span>{{$cartItems->totalQuantity}}</span></li>
							
							<!-- <li>ค่าส่งสินค้า <span>Free</span></li> -->
							<li>ราคารวม <span>{{number_format($cartItems->totalPrice)}}</span></li>
						</ul>
							<!-- <a class="btn btn-default update" href="">อัพเดท</a> -->
							<a class="btn btn-default check_out" href="/products/checkout">ชำระเงิน</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
@endsection