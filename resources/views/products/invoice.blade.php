<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link href="{{ asset('css/invoice.css') }}" rel="stylesheet">
<!DOCTYPE html>
<html>

<head>
    <style>

    </style>
</head>

<body>

    <div id="invoice">

        <div class="toolbar hidden-print">
            <div class="text-right">
                <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
            </div>
            <hr>
        </div>
        <div class="invoice overflow-auto">
            <div style="min-width: 600px">
                <header>
                    <div class="row">
                        <div class="col">
                            <a target="_blank" href="https://lobianijs.com">
                                <img src="{{asset('images/home/buasiamm.png')}}" data-holder-rendered="true" />
                            </a>
                        </div>
                        <div class="col company-details">
                            <h2 class="name">
                                <a target="" href="">
                                    บัวสยาม
                                </a>
                            </h2>
                            <div>29/12 ม.1 ถ.รังสิต-ปทุมธานี</div>
                            <div>ต.บ้านกลาง อ.เมือง จ.ปทุมธานี 12000</div>
                            <div>Tel/Fax:02-977-9912 Mobile: 081-307-2533/085-900-9753 </div>
                            <div>buasiam@hotmail.com</div>
                        </div>
                    </div>
                </header>
                <main>
                    <div class="row contacts">
                        <div class="col invoice-to">
                            <div class="text-gray-light">เรียน:</div>
                            <h2 class="to">บริษัท 11 house รับสร้างบ้าน</h2>
                            <div class="address">42/96 หมู่3 ต.บางพูด อ.ปากเกร็ด จ.นนทบุรี</div>
                            <div class="email"><a href="">nie.wachiranon@hotmail.com</a></div>
                        </div>
                        <div class="col invoice-details">
                            <h1 class="invoice-id">ใบเสร็จเลขที่ INVOICE00001</h1>
                            <div class="date">วันที่ออกใบเสร็จ: 1 มกราคม 2021</div>

                        </div>
                    </div>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th>ลำดับที่</th>
                                <th class="text-left">รายการสินค้า</th>
                                <th class="text-right">จำนวน</th>
                                <th class="text-right">ราคา/รายการ</th>
                                <th class="text-right">จำนวนเงิน(บาท)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="no">1</td>
                                <td class="text-left">
                                    เสาโรมันขนาด 25 ซม.
                                </td>
                                <td class="unit">2</td>
                                <td class="qty">600</td>
                                <td class="total">1200</td>
                            </tr>
                            <tr>
                                <td class="no">2</td>
                                <td class="text-left">
                                    คิ้วบัวปูนปั้นนาด 10 ซม.
                                </td>
                                <td class="unit">3</td>
                                <td class="qty">100</td>
                                <td class="total">300</td>
                            </tr>


                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">รวมทั้งสิ้น</td>
                                <td>1500 บาท</td>
                            </tr>


                        </tfoot>
                    </table>
                    <!-- <div class="thanks">Thank you!</div>
                    <div class="notices">
                        <div>NOTICE:</div>
                        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
                    </div> -->
                </main>
                <footer>

                </footer>
            </div>
            <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
            <div></div>
        </div>
    </div>
</body>

</html>