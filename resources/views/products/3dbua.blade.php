@extends("layouts.index")
@extends("layouts.custom")
@section("content")
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  
<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="left-sidebar">
						
    
					
						</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
<div class="table-responsive">
    <h2>สินค้าแบบลวดลายสามมิติ</h2>
    <form action="store3d" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
         <div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  ลวดลาย
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  <a class="dropdown-item" href="/products/pillar">เสาโรมัน</a>
    <a class="dropdown-item" href="/products/cutebua">คิ้วบัว</a>
    <a class="dropdown-item" href="/products/headpillar">หัวเสา</a>
 
  
  
  
    
   
  </div>
</div>
<br>
<br>

 <div class="form-group">
 <input type="hidden" id="status" name="status" value="รอการมอบหมายงาน">
 <input type="hidden" id="start" name="start" value="-">
 <input type="hidden" id="end" name="end" value="-">
 <input type="hidden" id="length" name="length" value="1">
 

    <label for="type">ชื่อสินค้า</label>
            <input type="text" class="form-control" name="nameorder" id="nameorder" placeholder="ชื่อ">
        </div>

    
       
        <div class="form-group">
            <label for="image">รูป</label>
            <input type="file" class="form-control"  name="image" id="image">
        </div> 

        <div class="form-group">
            <label for="type">ขนาด กว้าง x ยาว x สูง (เซนติเมตร)</label>
         
            <select name="cars" id="cars">
    <option value="volvo">15*20*30 ซม</option>
    <option value="saab">20*30*40 ซม</option>
    <option value="opel">30*40*50 ซม</option>
    
  </select>
            
        <div class="form-group">
            <label for="type">จำนวนที่สั่ง(ชิ้น)</label>
            <input type="text" class="form-control" name="amount" id="amount">
        </div>

      
  <div class="form-group">
            <label for="type">รายละอียดสินค้าเพิ่มเติม</label>
            <input type="text" class="form-control" name="description" id="description" placeholder="รายละเอียด">
        </div>
        
        <label for="type">ชื่อผู้สั่ง</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อ">
        </div>
        <label for="type">หมายเลขโทรศัพทฺ์</label>
            <input type="text" class="form-control" name="tel" id="tel" placeholder="หมายเลขติดต่อ">
        </div>


       
        
        <div class="form-group">

        <input  type="hidden" name="price" id="price_hidden">
            <label for="type">ประเมิณราคา(คลิ๊ก "คำนวณราคา" เพื่อคำนวณอัตโนมัติ)</label>
            <input type="text" class="form-control"  disabled  id="price">
        </div>
      
        
        <section id="do_action">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="total_area">
						<ul>
							<li>ราคาแบบพิมพ์ <span id = "pim"></span></li>
							
							<li>ราคารวม สินค้าที่ต้องชำระทั้งหมด
                            
                             <span id ="price2">
                             
                            </span></li>
						</ul>
                        <input class="btn btn-primary" type="button" onClick="calc()" value="คำนวณราคา" />
                        
							<!-- <button class="btn btn-default check_out" type="submit" name="submit">สั่งสินค้า</a> -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  
                            สั่งสินค้า
                            </button>          

                        <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">คุณตกลงที่จะสั่งสินค้า</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
          <p>ตรวจสอบข้อมูลที่สั่งว่าถูกต้อง สมบูรณ์ "ตกลง"</p>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
        <button type="submit" class="btn btn-secondary">ตกลง</button>
      </div>
    </div>
  </div>
</div>

					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
    </form>
    
    <!doctype html>
<html>
<head>
<meta charset="utf-8">


<script>

function calc(){
var width=document.getElementById('width').value;
var long=document.getElementById('long').value;
var high=document.getElementById('high').value;
var amount=document.getElementById('amount').value;

var sum=((Math.max(width,long,high)*20)*amount);
var pim = Math.max(width,long,high)*200;

var sumprice = sum+pim;
console.log(sumprice);
document.getElementById('amount').innerHTML=amount;

document.getElementById('price2').innerHTML=sumprice;
document.getElementById('pim').innerHTML=pim;
document.getElementById("price").value = sumprice;
document.getElementById("price_hidden").value = sumprice;
//document.getElementById("price").disabled = true;
return false

}
</script>
</head>
<body>
<form>
<!-- <p>ขนาดเสา<input type="text" id="width"/></p>
<p>ความยาว<input type="text" id="length" /></p>
<p>จำนวนชิ้น<input type="text"id="amount" /></p> -->
<!-- <input type="button" onClick="calc()" value="Calcular" /> -->
</form>
<div id="price"></div>
</body>
</html>
   
    <figure class="text-center"> 
   <figcaption class="figure-caption">รูปแบบตัวอย่าง</figcaption>
  <img src="{{asset('images/home/sing.jpg')}}" class="figure-img img-fluid rounded" alt="..." >
 
</figure> 
</div>






    
  
@endsection