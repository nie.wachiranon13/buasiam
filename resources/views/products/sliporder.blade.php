@extends("layouts.index")

@section("content")

<!DOCTYPE html>
<html>

<head>
    <style>

    </style>
</head>

<body>



    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="left-sidebar">

                </div>
            </div>
        </div>


        <div class="table-responsive">
            <h3>แนบหลักฐานการชำระเงิน(แบบสั่งซื้อ)</h3>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{route('sendslipOrder',$orders)}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}



                <div class="form-group">


                    <label for="type">หมายเลขใบสั่งซื้อ</label>
                    <input type="number" class="form-control" value="{{$orders->order_id}}" name="orderid" id="orderid" required disabled>

                </div>
                <div class="form-group">

                    <label for="type">จำนวนเงินที่ชำระ</label>
                    <input type="number" class="form-control" value="{{$orders->price}}" name="price" id="price" required disabled>

                </div>


                <div class="form-group">
                    <label for="image">รูป</label>
                    <input type="file" class="form-control" name="slip" id="slip" required>
                </div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    ยืนยัน
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">คุณตกลงที่จะส่งหลักฐานการโอนเงิน</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-body">
                                <p>ตรวจสอบข้อมูลที่สั่งว่าถูกต้อง สมบูรณ์ "ตกลง"</p>
                            </div>
                            <div class="modal-footer mf-4">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                <button type="submit" class="btn btn-secondary">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>





</body>

</html>
@endsection