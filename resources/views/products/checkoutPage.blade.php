@extends("layouts.index")
@section("content")
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="/">หน้าแรก</a></li>
                <li class="active">ชำระเงิน</li>
            </ol>
        </div>
        <div class="shopper-informations">
            <div class="row">

                @if($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
                @endif
                <div class="col-sm-12 clearfix">
                    <div class="bill-to">
                        <p> ข้อมูลการส่ง</p>
                        <div class="form-one">
                            <form action="/products/createOrder" method="post" class="form-group">
                                <!-- method post ต้องใส่ csrf field -->
                                @csrf
                                <label for="exampleInputPassword1">อีเมลติดต่อ</label>
                                <input type="text" name="email" value="{{ Auth::user()->email }}" placeholder="อีเมล*" required>
                                <label for="exampleInputPassword1">ชื่อ-นามสกุล , ชื่อบริษัท</label>
                                <input type="text" name="fname" value="{{ Auth::user()->name }}" placeholder="ชื่อ *">
                                <!-- <input type="text" name="lname" placeholder="{{ Auth::user()->name }}" required> -->
                                <label for="exampleInputPassword1">ที่อยู่</label>
                                <input type="text" name="address" value="{{ Auth::user()->address }}" placeholder="ที่อยุ่ *" required>

                                <label for="exampleInputPassword1">รหัสไปรษณีย์</label>
                                <input type="text" name="zip" value="{{ Auth::user()->postal }}" placeholder="รหัสไปรษณีย์ *" required>


                                <label for="exampleInputPassword1">เบอร์โทรศัพท์ติดต่อ</label>
                                <input type="text" name="phone" value="{{ Auth::user()->phone }}" placeholder="เบอร์โทรศัพท์ติดต่อ *" required>


                                <button class="btn btn-default check_out" type="submit" name="submit">ดำเนินการชำระเงิน</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection