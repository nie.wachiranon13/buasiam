@extends("layouts.index")
@section("content")
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="/">หน้าแรก</a></li>
                <li class="active">ชำระเงิน</li>
            </ol>
        </div>
            <div class="shopper-informations">
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <div class="bill-to">
                            <p> ข้อมูลการส่ง</p>
                            <div class="form-one">
                                <form action="/products/createOrder" method="post" class="form-group">
                        <!-- method post ต้องใส่ csrf field -->
                                {{csrf_field()}} 
                                    <input type="text" name="email" placeholder="อีเมล*" required>
                                    <input type="text" name="fname" placeholder="ชื่อ *" required>
                                    <input type="text" name="lname" placeholder="นามสกุล *"  required>
                                    <input type="text" name="address" placeholder="ที่อยู่ *" required>
                                    <input type="text" name="zip" placeholder="รหัสไปรษณีย์ *" required>
                                    <input type="text" name="phone" placeholder="เบอร์โทรศัพท์ *">
                                    <button class="btn btn-default check_out" type="submit" name="submit" >ดำเนินการชำระเงิน</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>

@endsection