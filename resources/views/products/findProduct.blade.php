@extends("layouts.index")
@section("content")
<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
                        
                    <h2>หมวดหมู่</h2>
						<div class="panel-group category-products" id="accordian">
			<!--category-productsr-->
			
					@foreach($categories as $category)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="/products/category/{{$category->id}}">{{($category->name)}}</a></h4>
								</div>
							</div>
						
						@endforeach	
						</div><!--/category-products-->

						<!-- <div class="price-range">
							<h2>Price Range</h2>
							<div class="well text-center">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
								 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
							</div>
						</div> -->
					</div>
				</div>

				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
				
						<h2 class="title text-center">{{$feature}}</h2>
						
						<!-- โชว์สินค้า -->
							@forelse($products as $product)
						<div class="col-sm-4">
							<div class="product-image-wrapper">

							<div class="single-products">
										<div class="productinfo text-center">
											<img src="{{asset('storage')}}/product_image/{{$product->image}}" alt=""width="250px" height="288px" />
											
											<h4>{{$product->name}}</h4>
											
											<h2>{{number_format($product->price)}} บาท</h2>
											
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>หยิบใส่รถเข็น</a>
											<p>จำนวนในคลังสินค้า: {{$product->stock}} รายการ</p>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<p>{{$product->name}}</p>
											<p>{{$product->description}}</p>
										
											<h2>{{number_format($product->price)}} บาท</h2>
												<a href="/products/addToCart/{{$product->id}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>หยิบใส่รถเข็น</a>
											</div>
										</div>
								</div>

								
								<div class="choose">
    									<ul class="nav nav-pills nav-justified">
	    										<li><a href="/products/details/{{$product->id}}"><i class="fa fa-info-circle"></i>รายละเอียดสินค้า</a></li>
	    										<!-- <li><a href="/products/addToCart/{{$product->id}}"><i class="fa fa-shopping-cart"></i>หยิบใส่รถเข็น</a></li> -->
    									</ul>
								   </div>
							</div>
                        </div>
                        @empty
                        <div class= "alert alert">
                            <p>ไม่มีสินค้าในหมวดหมู่ {{$feature}}</p> 

                        </div>

						@endforelse
					</div><!--features_items-->
					{{$products->links()}}
				</div>
			</div>
		</div>
	</section>
@endsection