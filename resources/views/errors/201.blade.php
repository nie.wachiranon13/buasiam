@extends('errors::minimal')

@section('title', __('Forbidden'))
@section('message')
<p>วัตถุดิบไม่เพียงพอ</p>
<a href="{{ route('material.index') }}" class="btn btn-info">เพิ่มวัตถุดิบ</a>
@endsection