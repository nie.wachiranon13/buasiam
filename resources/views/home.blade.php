@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">บัวสยาม</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                   <p> <strong>ชื่อ :</strong>{!! Auth::user()->name !!}</p>
                   <p> <strong>อีเมล :</strong>{!! Auth::user()->email !!}</p>                  
                    
                    
                    @if(Auth::user()->checkisAdmin())
                        <meta http-equiv="refresh" content="0; URL=/admin/createProduct" />
                    <!--<a href ="/admin/createProduct" class = "btn btn-primary">การจัดการสินค้า</a>-->
                    @else
                        <meta http-equiv="refresh" content="0; URL=/products" />
                    <!--<a href ="/products" class = "btn btn-success">หน้าแรก</a>-->
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
