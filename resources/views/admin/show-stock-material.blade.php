@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if($stock_materials[0])


        <div class ="table-responsive ">
        <h2>รายละเอียดสต๊อก : {{ $stock_materials[0]->name}} คงเหลือจำนวน {{number_format($stock_materials[0]->stock)}} กิโลกรัม</h2>
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">วันที่เพิ่มสต๊อก</th>
          
            <th scope="col">ชื่อสินค้า</th>
           
           
        
            <th scope="col">ราคาซื้อ(บาท/กก.)</th>
            
            <th scope="col">สต๊อกวัตถุดิบ(กก.)</th>
            <th scope="col">ราคารวม(บาท)</th>
            <th scope="col">จำนวนวัตถุดิบคงเหลือ(กก.)</th>
            <th scope="col"></th>
           
            </tr>
        </thead>
        <tbody>
        @foreach ($stock_materials as $stock_material)

          
            <tr>
            <th scope="row">{{DateThai($stock_material->created_at)}}</th>
           
            <td>{{$stock_material->name}}</td>
         

            
            <td>{{number_format($stock_material->price,2,'.',' ')}}</td>

            <td>{{number_format($stock_material->quantity)}}</td>
            <td>{{number_format($stock_material->price*$stock_material->quantity,2,'.',' ')}}</td>
           
            <td>{{number_format($stock_material->balance,2,'.',' ')}}</td>
        
            
        
            </tr>
            @endforeach
          
        </tbody>
        </table>
      


    </div>   
    @else
    <div class="alert alert-danger">
        <p>ไม่พบข้อมูล</p>
        </div> 
@endif

@endsection