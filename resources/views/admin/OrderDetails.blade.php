@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if($orderitems->count()>0)
        <div class ="table-responsive ">
        <h2>รายละเอียดการสั่งซื้อ</h2>
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">รหัสสินค้า</th>
            <th scope="col">ชื่อ</th>
            <th scope="col">ราคา</th>
            <th scope="col">จำนวน</th>
      
            
            </tr>
        </thead>
        <tbody>

        @foreach($orderitems as $orderitem)
      <tr>
        <th scope="row">{{$orderitem->item_id}}</th>
        <th scope="row">{{$orderitem->item_name}}</th>
        <th scope="row">{{$orderitem->item_price}}</th>
        <th scope="row">{{$orderitem->item_amount}}</th>
        
      </tr>
      @endforeach
          
        </tbody>
        </table>
        <a href="/admin/orders" class="btn btn-primary">กลับ</a>
     

    </div>   
    @else
    <div class=" alert alert-danger my-2">
        <p>ไม่พบข้อมูลสินค้าในใบสั่งซื้อ</p>
        </div> 
@endif

@endsection