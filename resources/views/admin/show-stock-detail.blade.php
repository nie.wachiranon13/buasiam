@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if($stocks[0])


        <div class ="table-responsive ">

      
        
        <h2>รายละเอียดสต๊อก : {{ $stocks[0]->name}} คงเหลือจำนวน {{number_format($stocks[0]->stock)}} รายการ</h2>
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">วันที่เพิ่มสต๊อก</th>
          
            <th scope="col">ชื่อสินค้า</th>
           
           
            <th scope="col">ราคาปูน(บาท)</th>
            <th scope="col">ราคาทราย(บาท)</th>
            <th scope="col">ราคาเหล็ก(บาท)</th>
            <th scope="col">ราคาแรงงาน(บาท)</th>
            <th scope="col">ราคาต้นทุน(บาท)</th>
          
            <th scope="col">สต๊อกสินค้า(รายการ)</th>
            
            <th scope="col"></th>
           
            </tr>
        </thead>
        <tbody>
        @foreach ($stocks as $stock)

          
            <tr>
            <th scope="row">{{DateThai($stock->created_at)}}</th>
           
            <td>{{$stock->name}}</td>
         

            
            <td>{{number_format($stock->cement_price)}}</td>
            <td>{{number_format($stock->soi_price)}}</td>
            <td>{{number_format($stock->iron_price)}}</td>
            <td>{{number_format($stock->work_price)}}</td>
            <td>{{number_format( ($stock->cement_price+$stock->soi_price+$stock->iron_price+$stock->work_price))}}</td>

            
            <td>{{number_format($stock->quantity)}}</td>
            
        
            </tr>
            @endforeach
          
        </tbody>
        </table>
        {{$stocks->links()}}


    </div>   
    @else
    <div class="alert alert-danger">
        <p>ไม่พบข้อมูล</p>
        </div> 
@endif


@endsection