@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="table-responsive">
    <h2>เพิ่มหมวดหมู่สินค้า</h2>
    <form action="/admin/createCategory" method="post">
        {{csrf_field()}} <!--ป้องกันการแฮคงุ้ยๆๆ -->
        <div class="form-group">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="หมวดหมู่สินค้า">
            </div>
            <div class="form-group">
            <label for="qtyperday">จำนวนที่ทำได้ต่อวัน</label>
            <input type="text" class="form-control" name="qtyperday" id="qtyperday" placeholder="วัน">
            </div>    
        <button type="submit" name="submit" class="btn btn-success">เพิ่มหมวดหมู่</button>
    </form>
</div>
@if($categories->count()>0)
        <div class ="table-responsive my-2">
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">รหัสสินค้า</th>
            <th scope="col">หมวดหมู่สินค้า</th>
            <th scope="col">จำนวนรายการสินค้าในหมวดหมู่</th>
            <th scope="col">แก้ไข</th>
            <th scope="col">ลบ</th>
            </tr>
        </thead>
        <tbody>
            @foreach($categories as $category)
            <tr>
            <th scope="row">{{$category->id}}</th>
            <td>{{$category->name}}</td>
            <td>{{$category->products->count()}}</td>
            <td>
                <a href="/admin/editCategory/{{$category->id}}" class ="btn btn-primary">แก้ไข</a>
            </td>
            <td>
            <a href="/admin/deleteCategory{{$category->id}}" onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่?')" class ="btn btn-danger">ลบ</a>
            </td>
            </tr>
            @endforeach
        </tbody>
        </table>
        
    </div>   
    @else
    
        
@endif

@endsection