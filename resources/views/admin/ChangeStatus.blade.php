@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
<div class="table-responsive">
<form action="{{ route('updateStatus',$id) }}" method="POST">
{{ csrf_field() }}
    <div class="form-group">
    <label for="exampleFormControlSelect1">สถานะ</label>
    <select class="form-control"  name="status" id="exampleFormControlSelect1" >
            <option value="ยังไม่ชำระเงิน">ยังไม่ชำระเงิน</option>
            <option value="ชำระเงินแล้ว">ชำระเงินแล้ว</option>
      
    </select>
  </div>
    
        <button type="submit" class="btn btn-primary">ยืนยัน</button>
    </form>
  
</div>

@endsection