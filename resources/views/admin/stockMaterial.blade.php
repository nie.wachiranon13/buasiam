@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if($materials->count()>0)


        <div class ="table-responsive ">
        <h2>เพิ่มสต๊อกวัตถุดิบ</h2>
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">รหัสวัตถุดิบ</th>
           
            <th scope="col">ชื่อวัตถุดิบ</th>
           
           
           
            <th scope="col">สต๊อกวัตถุดิบ</th>
            
            <th scope="col"></th>
           
            </tr>
        </thead>
        <tbody>
        @foreach ($materials as $material)

          
            <tr>
            <th scope="row">{{$material->id}}</th>
           
            <td>{{$material->name}}</td>
         

            
       
            <td>{{$material->stock}}</td>
            
          
            <td>
                <a href="addStockMaterial/{{$material->id}}" class ="btn btn-primary">เพิ่มสต๊อกวัตถุดิบ</a>
            </td> 
            
           
            </tr>
            @endforeach
          
        </tbody>
        </table>
        {{$materials->links()}}

    </div>   
    @else
    <div class="alert alert-danger">
        <p>ไม่พบข้อมูล</p>
        </div> 
@endif

@endsection