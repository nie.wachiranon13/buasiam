@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')



        <div class ="table-responsive ">
        @if($message = Session::get('succuss'))
        <div class="alert alert-success" role="alert">
         {{ $message }}
        </div>
        @endif
        <h2>ตารางรายได้รายการสั่งทำ</h2> 
        <html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 4 DatePicker</title>
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
  
</head>
<body>
        
<div class="input-group mb-3">
  <div id="sandbox-container" class="span5 col-md-3">
  <input type="text" class="form-control" >
  </div>
</div>
        <table class="table" id="order-table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">วันเดือนปี</th>
            <th scope="col">รายชื่อผู้สั่ง</th>
            <th scope="col">ขายได้(บาท)</th>
            <th scope="col">ต้นทุน(บาท)</th>
            
            <th scope="col">ผลรวม(บาท)</th>
            </tr>  
        </thead>
        <tbody>
        </tbody>
        </table>
     
       <div style="float: right;
    margin-top: 30px;
    margin-right: 75px;">
        <h3> รวม <span id="total"></span> บาท</h3>
       </div>

    </div>   

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>

function fill_datatable(filter_mouth = '',filter_year= '') {
    $('#order-table').DataTable({
        processing: false,
        serverSide: false,
        pageLength: 100,
        ajax: {
            url :'{!! route('datatables.orderPillars') !!}',
            type : "POST",
            data: {
                filter_mouth: filter_mouth,
                filter_year: filter_year,
            },
        },    
        columns: [
            { data: 'created_at', name: 'created_at' },
            { data: 'order_name', name: 'order_name' },
        
             { data: 'sell', name: 'sell' },
            { data: 'cost', name: 'cost' },
           
            { data: 'total', name: 'total'}
        ]
    });

    $.ajax({
        url: '{!! route('datatables.totalPillars') !!}',
        data: {
                filter_mouth: filter_mouth,
                filter_year: filter_year,
        },
        type : "POST",
        dataType: 'json', 
        success: function (data) {
            $('#total').text('');
            $('#total').append(data.total);
        },
        error: function (jqXhr, textStatus, errorMessage) { 
           
        }
    });
   
   }

    $('#sandbox-container input').datepicker({
    format: "mm/yyyy",
    endDate: "0d",
    startView: 1,
    language: "th",
    viewMode: "months", 
    minViewMode: "months",
    autoclose: true
   })
   .on("changeMonth", function(e) {
        console.log(e.date.getMonth())
        console.log(e.date.getFullYear())
        var filter_mouth = e.date.getMonth();
        var filter_year = e.date.getFullYear();
        if (filter_mouth != '' && filter_year != '') {
            $('#order-table').DataTable().destroy();
            fill_datatable(filter_mouth,filter_year);
        } else {
            $('#order-table').DataTable().destroy();
            fill_datatable();
        }
    });

    $(document).ready(function() {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
   });

   fill_datatable();
  
   
});

  </script>
  </body>
</html>

@endsection

