@extends('layouts.admin')
<!-- ดึงlayoutของadminมา -->
@section('body')

<h1>รหัสสั่งทำที่ : {{ $invoice->ref_id }}</h1>

<form action="{{ route('updateStatusCus') }}" method="post">
    @csrf
    <input type="hidden" name="invoice_id" value="{{ $invoice->id }}">
    <div class="table-responsive ">
        <h3>รายละเอียดใบสั่งซื้อ</h2>
            <table class="table mr-4">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ชื่อ</th>
                        <th scope="col">รูป</th>

                        <th scope="col">จำนวน</th>

                        <th scope="col">ราคา</th>
                        <th scope="col">ราคาเพิ่มเติม</th>
                        <th scope="col">หมายเหตุเพิ่มเติม</th>
                        <th scope="col"></th>

                    </tr>
                </thead>
                <tbody>

                    @foreach($invoice_details as $item)
                    <tr>

                        <th scope="row">
                            <input type="hidden" name="productcustom_id[]" value="{{ $item->productcustom_id }}">
                            <input type="hidden" name="id[]" value="{{ $item->id }}">
                            {{ $item->product->name ??  '' }}
                        </th>
                        <th scope="row">
                            <img src="{{ asset($item->image)}}" width="120" height="auto">
                        </th>
                        <th scope="row">{{ number_format($item->amount) }}</th>
                        <th scope="row">{{ number_format($item->total_price) }}</th>
                        <th scope="col-2">
                            <div class="form-group">
                                <input type="number" min="0" name="other_price[]" class="form-control" {{ $invoice->status == 'รอการยืนยัน' ? '' : 'disabled' }} value="{{ $item->other_price }}">
                            </div>
                        </th>
                        <th>
                            <div class="form-group">
                                <textarea class="form-control" name="note[]" rows="3" placeholder="" {{ $invoice->status == 'รอการยืนยัน' ? '' : 'disabled' }}>{!! $item->note !!}</textarea>
                            </div>
                        </th>

                    </tr>
                    @endforeach
                </tbody>
            </table>

    </div>
    <div class="form-inline  mb-4">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">สถานะใบสั่งซื้อ</label>
        <select name="status" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">

            <option value="รอการยืนยัน" {{ $invoice->status == 'รอการยืนยัน' ? 'selected' : '' }}>รอการยืนยัน</option>
            <option value="ยืนยันการสั่งทำ" {{ $invoice->status == 'ยืนยันการสั่งทำ' ? 'selected' : '' }}>ยืนยันการสั่งทำ</option>
            <option value="เสร็จสิ้น" {{ $invoice->status == 'เสร็จสิ้น' ? 'selected' : '' }}>เสร็จสิ้น</option>
        </select>



        <!-- <button type="submit" class="btn btn-primary my-1">Submit</button> -->

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
            ยืนยัน
        </button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">คุณต้องการยืนยันใช่หรือไม่</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    คุณแน่ใจใช่หรือไม่ว่าต้องการเปลี่ยนสถานะการชำระเงิน
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-primary">ใช่</button>
                </div>
            </div>
        </div>
    </div>


</form>



@endsection