@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')



        <div class ="table-responsive ">
        @if($message = Session::get('succuss'))
        <div class="alert alert-success" role="alert">
         {{ $message }}
        </div>
        @endif
        <h2>รายการวัตถุดิบ</h2>
        <table class="table">
        <thead class="thead-dark">

       
            <tr>
            <th scope="col">รหัสวัตถุดิบ</th>
            
            <th scope="col">ชื่อวัตถุดิบ</th>
        
         
      
            <th scope="col">สต๊อกวัตถุดิบ(กก.)</th>
            
            <th scope="col">ดูรายละเอียดการเพิ่มสต๊อก</th>
            <th scope="col">เพิ่มสต๊อกวัตถุดิบ</th>
            
          
            </tr>
        </thead>
        @foreach ($materials as $material)
        <tbody>
     

          
            <tr>
            <th scope="row">{{$material->id}}</th>
            
          

           
           
            <td>{{$material->name}}</td>
            <td>{{$material->stock}}</td>
            
           
            <td><a href="{{ route('showStockMaterialDetail',$material->id)}}" class ="btn btn-info">ดูรายละเอียด</a></td>
            
           
            <td>
                <a href="addStockMaterial/{{$material->id}}" class ="btn btn-primary">เพิ่มสต๊อกวัตถุดิบ</a>
            </td> 
            </tr>
    
            @endforeach
        </tbody>
        </table>
        {{$materials->links()}}
       

    </div>   
   


@endsection