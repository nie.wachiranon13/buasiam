@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')



        <div class ="table-responsive ">
        @if($message = Session::get('succuss'))
        <div class="alert alert-success" role="alert">
         {{ $message }}
        </div>
        @endif

        <h2>รายชื่อพนักงาน</h2>
        <td><a href="employeeForm" class ="btn btn-info">เพิ่มข้อมูลพนักงาน</a></td>
        <table class="table">
        <thead class="thead-dark">

       
            <tr>
            <th scope="col">รหัสพนักงาน</th>
            <th scope="col">เลขบัตรประชาชน</th>
            
            <th scope="col">ชื่อ</th>
        
         
      
            <th scope="col">นามสกุล</th>
            
            <th scope="col">ที่อยู่</th>
            <th scope="col">ตำแหน่ง</th>
        
            
            <th scope="col">แก้ไข</th>
            <th scope="col">ลบ</th>
            </tr>
        </thead>
        
        <tbody>
     
                @foreach($employees as $employee)
          
            <tr>
           
          

           
           
            <td>{{$employee->id}}</td>
            <td>{{$employee->idcard}}</td>
            <td>{{$employee->fname}}</td>
            <td>{{$employee->lname}}</td>
            <td>{{$employee->address}}</td>
            <td>{{$employee->position}}</td>
           
            
           
            <td>
                <a href="editEmp/{{$employee->id}}" class ="btn btn-primary">แก้ไข</a>
            </td> 
            
            <td>
            <a href="" onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่?')" class ="btn btn-danger">ลบ</a>
            </td>
            </tr>
    
         @endforeach
        </tbody>
        </table>
       

    </div>   
   


@endsection