@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<div class="table-responsive">
    <h2>แก้ไขข้อมูลพนักงาน</h2>
    <form action="{{route('updateEmp',$id)}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="fname" id="fname" placeholder="ชื่อ">
        </div>
        <div class="form-group">
            <label for="name">นามสกุล</label>
            <input type="text" class="form-control" name="lname" id="lname" placeholder="นามสกุล" >
        </div>
        <div class="form-group">
            <label for="name">เลขบัตรประชาชน</label>
            <input type="text" class="form-control" name="idcard" id="idcard" placeholder="เลขบัตรประชาชน" >
        </div>
        <div class="form-group">
            <label for="name">ที่อยู่</label>
            <input type="text" class="form-control" name="address" id="address" placeholder="เลขที่บ้าน ถนน จังหวัด" >
        </div>
        <div class="form-group">
            <label for="name">ตำแหน่ง</label>
            <input type="text" class="form-control" name="position" id="position" placeholder="" > 
        </div>
        
        <input type="hidden" name="salary" value="0" class="form-controller">
        <button type="submit" name="submit" class="btn btn-success">ยืนยัน</button>
    </form>
</div>

@endsection