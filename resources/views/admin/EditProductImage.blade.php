 @extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<div class="table-responsive">
    <h2>รูปภาพ</h2>
    <div>
        <img src ="{{asset('storage')}}/product_image/{{$product->image}}"alt="">
        
    </div>
    <form action="/admin/updateProductImage/{{$product->id}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
      
        <div class="form-group">
            <label for="image">รูป</label>
            <input type="file" class="form-control"  name="image" id="image">
        </div>
   
        <button type="submit" name="submit" class="btn btn-success">ยืนยัน</button>
    </form>
</div>

@endsection