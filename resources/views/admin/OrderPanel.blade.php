@extends('layouts.admin')
<!-- ดึงlayoutของadminมา -->
@section('body')



<!DOCTYPE html>
<html>

<head>
    <style>
        /* Style the Image Used to Trigger the Modal */
        #myImg {
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s;
        }

        #myImg:hover {
            opacity: 0.7;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.9);
            /* Black w/ opacity */
        }

        /* Modal Content (Image) */
        .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
        }

        /* Caption of Modal Image (Image Text) - Same Width as the Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }

        /* Add Animation - Zoom in the Modal */
        .modal-content,
        #caption {
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @keyframes zoom {
            from {
                transform: scale(0)
            }

            to {
                transform: scale(1)
            }
        }

        /* The Close Button */
        .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px) {
            .modal-content {
                width: 100%;
            }
        }
    </style>

</head>

<body>

    @if($orders->count()>0)
    <div class="table-responsive ">
        <h2>รายการสั่งซื้อ</h2>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">รหัสสั่งซื้อ</th>
                    <th scope="col">วันที่สั่งซื้อ</th>
                    <th scope="col">ผู้ใช้งาน</th>
                    <th scope="col">ราคา</th>


                    <th scope="col">สถานะ</th>
                    <th scope="col">สลิปการโอนเงิน</th>

                    <th scope="col">รายละเอียดใบสั่งซื้อ</th>
                    <th>
                    </th>


                </tr>
            </thead>
            <tbody>

                @foreach($orders as $order)
                <tr>
                    <th scope="row">{{$order->order_id}}</th>
                    <th scope="row">{{DateThai($order->created_at)}}</th>
                    <th scope="row">{{$order->fname}}</th>
                    <th scope="row">{{number_format($order->price)}}</th>



                    <th scope="row">
                        <span class="
              @if($order->status=='ยังไม่ชำระเงิน')
                  badge badge-danger
              @else
                  badge badge-success
              @endif
          ">{{$order->status}}</span>
                    </th>
                    <td>

                    </td>
                    <td>
                        <a href="/admin/orders/detail/{{$order->order_id}}" class="btn btn-info">รายละเอียด</a>
                    </td>
                    <td>
                        <a href="editStatus/{{$order->order_id}}" class="btn btn-warning">เปลี่ยนสถานะการชำระเงิน</a>
                    </td>
                    <td>

                    </td>
                </tr>
                @endforeach
                <!-- Trigger the Modal -->
                <img id="myImg" src="https://s3.amazonaws.com/ooomf-com-files/deYU3EyQP9cN23moYfLw_Dandelion.jpg" alt="Snow" style="width:100%;max-width:300px">

                <!-- The Modal -->
                <div id="myModal" class="modal">

                    <!-- The Close Button -->
                    <span class="close">&times;</span>

                    <!-- Modal Content (The Image) -->
                    <img class="modal-content" id="img01">

                    <!-- Modal Caption (Image Text) -->
                    <div id="caption"></div>
                </div>

            </tbody>
        </table>
        {{$orders->links()}}

    </div>
    @else
    <div class="alert alert-danger">
        <p>ไม่พบข้อมูลการสั่งซื้อ</p>
    </div>
    @endif

</body>
<script>
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById("myImg");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function() {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
</script>

</html>
@endsection