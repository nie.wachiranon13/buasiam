 @extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<div class="table-responsive">
    <h2>เพิ่มสินค้าใหม่</h2>
    <form action="createProduct" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อ">
        </div>
        <div class="form-group">
            <label for="description">รายละเอียด</label>
            <input type="text" class="form-control" name="description" id="description" placeholder="รายละเอียด">
        </div>
        

        <div class="form-group">
            <label for="description">ขนาดความกว้าง</label>
            <input type="number" class="form-control" name="width" id="width" placeholder="เซนติเมตร">
        </div>
        <div class="form-group">
            <label for="image">รูป</label>
            <input type="file" class="form-control"  name="image" id="image">
        </div>
        <div class="form-group">
            <label for="type">หมวดหมู่</label>
            <select class="form-control" name="category">
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                   
                    @endforeach
            </select>
        </div>
        
        <div class="form-group">
            <label for="type">ราคาขาย/รายการ)</label>
            <input type="number" class="form-control" name="price" id="price" placeholder="ราคาขาย">
        </div>
        <hr>
        <h4>สัดส่วนที่ใช้ในการทำ</h4>
        <div class="form-group">
            <label for="cement">ใช้ปูน(กก./รายการ)</label>
            <input type="number" class="form-control" name="cement" step="0.01" id="cement" placeholder="ใช้ปูน">
        </div>
        <div class="form-group">
            <label for="soi">ใช้ทราย(กก./รายการ)</label>
            <input type="number" class="form-control" name="soi" step="0.01" id="soi" placeholder="ใช้ทราย">
        </div>
        <div class="form-group">
            <label for="iron">ใช้เหล็ก(กก./รายการ)</label>
            <input type="number" class="form-control" name="iron" step="0.01" id="iron" placeholder="ใช้เหล็ก">
        </div>
        <h4>ค่าแรงต่อรายการ</h4>
        <div class="form-group">
            <label for="workprice">ราคาค่าแรง(บาท/รายการ)</label>
            <input type="number" class="form-control" name="workprice" step="0.01" id="workprice" placeholder="บาท">
        </div>
        <!-- <div class="form-group">
            <label for="type">จำนวนในคลังสินค้า</label>
            <input type="number" class="form-control" name="stock" id="stock" placeholder="จำนวน">
        </div> -->
        <input type="hidden" name="stock" value="0" class="form-controller">
        <button type="submit" name="submit" class="btn btn-success">ยืนยัน</button>
    </form>
</div>

@endsection