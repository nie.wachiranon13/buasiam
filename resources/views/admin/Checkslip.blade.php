@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if($slips->count()>0)
        <div class ="table-responsive ">
        <h2>การแจ้งการชำระเงิน</h2>
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">รูป</th>
            <th scope="col">รหัสใบสั่งซื้อ</th>
            <th scope="col">ราคา</th>
         
      
            
            </tr>
        </thead>
        <tbody>

        @foreach($slips as $slip)
      <tr>
      <th scope="row"> <img src ="{{asset('storage')}}/create_custom_image/{{$slip->image}}" alt="" width="350" height="350"></th>
   
   
        <th scope="row">{{$slip->orderid}}</th>
        <th scope="row">{{$slip->price}}</th>
     
        
      </tr>
      @endforeach
          
        </tbody>
        </table>
        <a href="/admin/orders" class="btn btn-primary">กลับ</a>
     

    </div>   
    @else
    <div class=" alert alert-danger my-2">
        <p>ไม่พบหลักฐานการชำระเงิน</p>
        </div> 
@endif

@endsection