@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if($products->count()>0)


        <div class ="table-responsive ">
        <h2>เพิ่มสต๊อกสินค้า</h2>
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">รหัสสินค้า</th>
            <th scope="col">รูปภาพ</th>
            <th scope="col">ชื่อสินค้า</th>
           
           
            <th scope="col">ราคาขาย/เมตร,ชิ้น</th>
            <th scope="col">สต๊อกสินค้า(รายการ)</th>
            
            <th scope="col"></th>
           
            </tr>
        </thead>
        <tbody>
        @foreach ($products as $product)

          
            <tr>
            <th scope="row">{{$product->id}}</th>
            <td>
                <img src ="{{asset('storage')}}/product_image/{{$product->image}}" alt="">
            </td>
            <td>{{$product->name}}</td>
         

            
            <td>{{number_format($product->price)}}</td>
            <td>{{$product->stock}}</td>
            
          
            <td>
                <a href="/admin/addStock/{{$product->id}}" class ="btn btn-primary">เพิ่มสต๊อกสินค้า</a>
            </td> 
            
           
            </tr>
            @endforeach
          
        </tbody>
        </table>
        {{$products->links()}}

    </div>   
    @else
    <div class="alert alert-danger">
        <p>ไม่พบข้อมูล</p>
        </div> 
@endif

@endsection