@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<div class="table-responsive">
        <h2>เพิ่มสต๊อกวัตถุดิบ</h2>
        <form action="{{route('insertStockMaterial')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="material_id" value="{{$material->id}}" class="form-controller">
        
        <div class="form-group">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="{{$material->name}}" disabled>
        </div>
        <div class="form-group">
            <label for="price">ราคา(บาท/กก.)</label>
            <input type="number" class="form-control" name="price" step="0.01" id="price" placeholder="ราคา">
        </div>
       
        <div class="form-group">
            <label for="type">เพิ่มวัตถุดิบจำนวน(กก.)</label>
            <input type="number" class="form-control" name="quantity" id="quantity" placeholder="จำนวน">
        </div>
        <button type="submit" name="submit" class="btn btn-success">ยืนยัน</button>
    </form>
  
</div>

  @endsection