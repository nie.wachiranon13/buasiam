@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<div class="container">
    <h2>จัดการงาน</h2>

    <form action="{{route('updateWord')}}" method="POST">
        @csrf
        <div class="form-group">
            <input type="hidden" name="id" value="{{$productcustompillars->id}}" class="form-controller">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$productcustompillars->nameorder}}" disabled>
        </div>
        <div class="form-group">
          ผู้รับงาน
        </div>
        @foreach($users as $user)
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="user{{$user->id}}" name="user[]" value="{{$user->id}}">
            <label class="form-check-label" for="user{{$user->id}}">{{$user->fname." ".$user->lname}}</label>
        </div>
       @endforeach

       <div class="form-row">
        <div class="col">
        <input type="datetime-local" class="form-control" name="start" placeholder="วันที่เริ่มงาน" require>
        </div>
        <div class="col">
        <input type="datetime-local" class="form-control" name="end" placeholder="วันที่เสร็จ" require>
        </div>
    </div>

    <div class="form-group">
    <label for="exampleFormControlSelect1">สถานะ</label>
    <select class="form-control"  name="status" id="exampleFormControlSelect1" require>
            <option value="">สถานะการทำงาน</option>
            <option value="รอการมอบหมายงาน">รอการมอบหมายงาน</option>
            <option value="กำลังทำ">กำลังทำ</option>
            <option value="เสร็จสิ้น">เสร็จสิ้น</option>
            <option value="ยกเลิกงาน">ยกเลิกงาน</option>
    </select>
  </div>
    
        <button type="submit" class="btn btn-primary">ยืนยัน</button>
    </form>
  
</div>

@endsection