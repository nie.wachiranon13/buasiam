
<div class="modal fade" id="modalCalendar" tabindex="-1" role="dialog" aria-labelledby="titleModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titleModal">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
        <div class="form-group row">
    <label for="title" class="col-sm-2 col-form-label">เรื่อง</label>
    <div class="col-sm-10">
      <input type="text"  name= "title" class="form-control" id="title">
    </div>
    <div class="form-group row">
    <label for="start" class="col-sm-2 col-form-label">เริ่มวันที่</label>
    <div class="col-sm-10">
      <input type="text"  name= "start" class="form-control" id="start">
    </div>
    <div class="form-group row">
    <label for="end" class="col-sm-2 col-form-label">เสร็จวันที่</label>
    <div class="col-sm-10">
      <input type="text"  name= "end" class="form-control" id="end">
    </div>
    <div class="form-group row">
    <label for="color" class="col-sm-2 col-form-label">เสร็จวันที่</label>
    <div class="col-sm-10">
      <input type="color"  name= "color" class="form-control" id="color">
    </div>
    <div class="form-group row">
    <label for="description" class="col-sm-2 col-form-label">เสร็จวันที่</label>
    <div class="col-sm-10">
    <textarea name="description" id="description" cols="40" rows="4"></textarea>
      
    </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger deleteEvent">Save changes</button>
        <button type="button" class="btn btn-primary saveEvent">Save changes</button>
      </div>
    </div>
  </div>
</div>