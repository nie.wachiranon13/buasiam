@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<div class="table-responsive">
        <h2>สั่งเพิ่มสต๊อกสินค้า</h2>
        <form action="{{route('insertStock')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="product_id" value="{{$product->id}}" class="form-controller">
        <img src ="{{asset('storage')}}/product_image/{{$product->image}}" alt="">
        <div class="form-group">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="{{$product->name}}" disabled>
        </div>
        <!-- <div class="form-group">
            <label for="cement_price">ราคาปูน/หน่วยสินค้า</label>
            <input type="number" class="form-control" name="cement_price" id="cement_price" placeholder="ราคา">
        </div>
        <div class="form-group">
            <label for="soi_price">ราคาทราย/หน่วยสินค้า</label>
            <input type="number" class="form-control"  name="soi_price" id="soi_price" placeholder="ราคา">
        </div>
       
        <div class="form-group">
            <label for="type">ราคาเหล็ก/หน่วยสินค้า</label>
            <input type="number" class="form-control" name="iron_price" id="iron_price" placeholder="ราคา">
        </div> -->
        <div class="form-group">
            <label for="type">เพิ่มสินค้าจำนวน(รายการ)</label>
            <input type="number" class="form-control" name="stock" id="stock" placeholder="จำนวน" required min="1">
        </div>
        <button type="submit" name="submit" class="btn btn-success">ยืนยัน</button>
    </form>
  
</div>

  @endsection