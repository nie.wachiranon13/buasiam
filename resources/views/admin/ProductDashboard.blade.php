@extends('layouts.admin')
<!-- ดึงlayoutของadminมา -->
@section('body')
@if($products->count()>0)


<div class="table-responsive ">
    @if($message = Session::get('succuss'))
    <div class="alert alert-success" role="alert">
        {{ $message }}
    </div>
    @endif
    <h2>รายการสินค้า</h2>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">รหัสสินค้า</th>
                <th scope="col">รูปภาพ</th>
                <th scope="col">ชื่อสินค้า</th>
                <th scope="col">รายละเอียด</th>
                <th scope="col">ขนาดความกว้าง(ซม.)</th>
                <th scope="col">ราคา(บาท)</th>
                <th scope="col">หมวดหมู่</th>

                <th scope="col">สต๊อกสินค้า(รายการ)</th>
                <th scope="col">สั่งเพิ่มสต๊อกสินค้า</th>

                <th scope="col">ดูรายละเอียดการเพิ่มสต๊อก</th>
                <!-- <th scope="col">แก้ไขรูปภาพ</th> -->
                <th scope="col">แก้ไข</th>
                <th scope="col">ลบ</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)


            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>
                    <img src="{{ Storage::url('product_image/'.$product->image) }}" alt="">
                </td>
                <td>{{$product->name}}</td>
                <td>{{$product->description}}</td>
                <td>{{$product->width}}</td>
                <td>{{number_format($product->price)}}</td>
                <td>
                    {{$product->category->name}}
                </td>

                <td>{{$product->stock}}</td>
                <td>
                    <a href="/admin/addStock/{{$product->id}}" class="btn btn-success">เพิ่มสต๊อกสินค้า</a>
                </td>


                <td><a href="{{ route('showStockDetail',$product->id)}}" class="btn btn-info">ดูรายละเอียด</a></td>

                <!-- <td><a href="/admin/editProductImage/{{$product->id}}" class ="btn btn-success">แก้ไขรูปภาพ</a></td> -->
                <td>
                    <a href="/admin/editProduct/{{$product->id}}" class="btn btn-warning">แก้ไข</a>
                </td>

                <td>
                    <a href="/admin/deleteProduct/{{$product->id}}" onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่?')" class="btn btn-danger">ลบ</a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
    {{$products->links()}}

</div>
@else
<div class="alert alert-danger">
    <p>ไม่พบข้อมูล</p>
</div>
@endif

@endsection