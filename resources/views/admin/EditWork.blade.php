@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<div class="container">
    <h2>จัดการงาน</h2>

    <form action="{{route('reworkWord')}}" method="POST">
        @csrf
        <div class="form-group">
            <input type="hidden" name="id" value="{{$productcustompillars->id}}" class="form-controller">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$productcustompillars->nameorder}}" disabled>
            
        </div>
        <div class="form-group">
            จำนวนรายการที่ต้องทำ :
            <span>{{number_format(($productcustompillars->length*$productcustompillars->amount))}}รายการ</span>
        </div>
        <div class="form-group">
            ใช้เวลาทำประมาณ :
            <span>{{$day}} วัน</span>
        </div>   
     
        <br>
         <table class="table table-bordered">
        <thead>
            <tr class="d-flex"> 
            ตารางงานพนักงาน
                <th class="col-2">พนักงาน</th>
                <th class="col-5">สามารถเริ่มงานได้เร็วสุด</th>
                <th class="col-5">วันที่จะทำงานเสร็จ</th>
            </tr>
        </thead>
        <tbody>
       
            @foreach($emplyeework as $emp)
            @php
            $emps = App\Employee::all();
            $cat = App\Category::find($productcustompillars->category_id);

            
            $work = App\productcustompillar::where('status','กำลังทำ')->where('end','>=',now())->orderBy('end', 'asc')->first();
            $work = App\productcustompillar::
            join('jobs', 'jobs.job_id', '=', 'productcustompillars.id')
            ->leftJoin('employees', 'employees.id', '=', 'jobs.user_id')
            ->where('status','กำลังทำ')->where('end','>=',now())->orderBy('end', 'asc')->get();

            $check = false;
            $num = 0;  
            if(count($emps) > 0){ 
                foreach($emps as $item){
                    if(count($work) > 0){
                        foreach($work as $job){  
                            if($item->id === $job->user_id){
                                $num += 1;
                                $check = true;
                            } 
                        }
                    }
                }
            }
            
            if($check && ($num != count($emps))){
                $day =  ceil($productcustompillars->quantity/$cat->qtyperday)+2;
            }else if(!empty($work)){
                $date1= now();
                $date2= date_create($productcustompillars->end);
                $diff=date_diff($date1,$date2);
                $numdiff = $diff->format('%d') + 2;
                $day = ceil(($emp->amount *  $emp->length) / $cat->qtyperday);

            }else{
                $day =  ($emp->amount *  $emp->length)/$cat->qtyperday ;    
            }

            if($emp->category_id == 4){
                $day = (ceil(($emp->amount) / $cat->qtyperday)) + 7;
              
            }

            $day += 1;
       
            @endphp    
            <tr class="d-flex">
                <td class="col-sm-2">{{ $emp->fname." ".$emp->lname}}</td>
                <td class="col-sm-5">{{  date('m-d-Y',strtotime($emp->end. "+1 days"))  }}</td>
                <td class="col-sm-5">{{  date('m-d-Y',strtotime($emp->end. "+".$day." days"))  }}</td>
           
            </tr>
            @endforeach
        </tbody>  
    </table>
        <div class="form-group">
          เลือกผู้รับงาน
        </div>
     
        @foreach($users as $user)
  
          @php
          $selected = '';
          foreach($jobs as $user_job){
             if($user->id === $user_job->user_id){
                $selected ='checked';
             }
            }
          @endphp
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="user{{$user->id}}" name="user[]" value="{{$user->id}}" {{ $selected }}>
            <label class="form-check-label" for="user{{$user->id}}">{{$user->fname." ".$user->lname }}</label>
        </div>
       @endforeach

       <div class="form-row">
        <div class="col">
        <input type="date" class="form-control" name="start" placeholder="วันที่เริ่มงาน" require value="{{ $productcustompillars->start }}">
        </div>
        <div class="col">
        <input type="date" class="form-control" name="end" placeholder="วันที่เสร็จ" require value="{{ $productcustompillars->end}}" >
        </div>
    </div>

    <div class="form-group">
           
           <label for="name">ราคาปูน</label>
           <input type="text" class="form-control" name="cement_price" id="cement_price" value="{{ $productcustompillars->cement_price}}" 
           >
   </div>
    <div class="form-group">
           
            <label for="name">ราคาทราย</label>
            <input type="text" class="form-control" name="soi_price" id="soi_price" value="{{ $productcustompillars->soi_price}}"
           >
            <!-- {{ !empty($productcustompillars->soi_price) ? 'disabled' : '' }} -->
    </div>

    <div class="form-group">
           
            <label for="name">ราคาเหล็ก</label>
            <input type="text" class="form-control" name="iron_price" id="iron_price" value="{{ $productcustompillars->iron_price}}"
            >
    </div>     
    <div class="form-group">
           
            <label for="name">ราคาค่าแรง</label>
            <input type="text" class="form-control" name="work_price" id="work_price" value="{{ $productcustompillars->work_price}}"
            >
    </div>    
    <div class="form-group">
    <label for="exampleFormControlSelect1">สถานะ</label>
    <select class="form-control"  name="status" id="exampleFormControlSelect1" require>
            <option value="">สถานะการทำงาน</option>
            <option value="รอการมอบหมายงาน" {{ $productcustompillars->status == "รอการมอบหมายงาน" ? "selected" : "" }}>รอการมอบหมายงาน</option>
            <option value="กำลังทำ" {{ $productcustompillars->status == "กำลังทำ" ? "selected" : "" }}>กำลังทำ</option>
            <option value="เสร็จสิ้น" {{ $productcustompillars->status == "เสร็จสิ้น" ? "selected" : "" }}>เสร็จสิ้น</option>
            <option value="ยกเลิกงาน" {{ $productcustompillars->status == "ยกเลิกงาน" ? "selected" : "" }}>ยกเลิกงาน</option>
    </select>
  </div>
    
        <button type="submit" class="btn btn-primary">ยืนยัน</button>
    </form>

</div>

@endsection