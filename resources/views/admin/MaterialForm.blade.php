@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<div class="table-responsive">
    <h2>เพิ่มวัตถุดิบใหม่</h2>
    <form action="createMaterial" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อ">
        </div>
       
       <input type="hidden" name="stock" value="0" class="form-controller">
        
 
        
        
        <button type="submit" name="submit" class="btn btn-success">ยืนยัน</button>
    </form>
</div>

@endsection