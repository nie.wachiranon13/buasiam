 @extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="table-responsive">
    <h2>แก้ไขหมวดหมู่สินค้า</h2>
    <form action="/admin/updateCategory/{{$category->id}}" method="post">
        {{csrf_field()}} <!--ป้องกันการแฮคงุ้ยๆๆ -->
        <div class="form-group">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="หมวดหมู่สินค้า" value = "{{$category->name}}">
            </div>
            <div class="form-group">
            <label for="qtyperday">จำนวนที่ทำได้ต่อวัน</label>
            <input type="text" class="form-control" name="qtyperday" id="qtyperday" placeholder="วัน" value = "{{$category->qtyperday}}" >
            </div>   
        <button type="submit" name="submit" class="btn btn-primary">อัพเดท</button>
    </form>
</div>
@endsection