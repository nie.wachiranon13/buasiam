@extends('layouts.admin')
<!-- ดึงlayoutของadminมา -->
@section('body')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="table-responsive">
    <h2>แก้ไขสินค้า</h2>
    <form action="/admin/updateProduct/{{$product->id}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">ชื่อ</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อ" value="{{$product->name}}">
        </div>
        <div class="form-group">
            <label for="description">รายละเอียด</label>
            <input type="text" class="form-control" name="description" id="description" placeholder="รายละเอียด" value="{{$product->description}}">
        </div>
        <div class="form-group">
            <label for="type">หมวดหมู่</label>
            <select class="form-control" name="category">

                @foreach($categories as $category)
                <option value="{{$category->id}}" @if($category->id==$product->category_id)
                    selected
                    @endif
                    >{{$category->name}}</option>

                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="type">ราคา</label>
            <input type="text" class="form-control" name="price" id="price" placeholder="ราคา" value="{{$product->price}}" @if($product->stock > 0) {{ "disabled" }} @endif>
        </div>
        <div class="form-group">
            <label for="name">คลังสินค้า</label>
            <input type="text" class="form-control" name="stock" id="stock" placeholder="จำนวน" value="{{$product->stock}}" readonly>
        </div>
        <div class="table-responsive">
            <h2>รูปภาพ</h2>
            <div>
                <img src="{{ Storage::url('product_image/'.$product->image) }}" alt="">

            </div>


            <div class="form-group">
                <label for="image">รูป</label>
                <input type="file" class="form-control" name="image" id="image">
            </div>



        </div>
        <button type="submit" name="submit" class="btn btn-success">ยืนยัน</button>
    </form>


</div>

@endsection