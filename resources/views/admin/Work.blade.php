@extends('layouts.admin') <!-- ดึงlayoutของadminมา -->
@section('body')



        <div class ="table-responsive ">
        @if($message = Session::get('succuss'))
        <div class="alert alert-success" role="alert">
         {{ $message }}
        </div>
        @endif
        <h2>ตารางการทำงาน</h2>
        
        
  

     
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">รหัสงาน</th>
            <th scope="col">ชื่องาน</th>
            <th scope="col">ผู้รับงาน</th>
            <th scope="col">ขนาด (เซนติเมตร)</th>
          
            <th scope="col">จำนวน</th>
            <th scope="col">วันเริ่มงาน</th>
            <th scope="col">วันเสร็จสิ้นงาน</th>
           
            <th scope="col">สถานะ</th>
            <th></th>
          
            </tr>
        </thead>
        <tbody>
        @foreach ($productcustompillars as $productcustompillar)

          
<tr>
<td>{{$productcustompillar->id}}</td>
<!-- <th scope="row"> <img src ="{{asset('storage')}}/create_custom_image/{{$productcustompillar->image}}" alt=""></th> -->

<td>{{$productcustompillar->nameorder}}</td>

<td>
    
{{ $productcustompillar->my_users}}</td>


<td>{{$productcustompillar->width}}</td>        


<td>{{$productcustompillar->amount * $productcustompillar->length}}</td>
<td>
{{$productcustompillar->start}}</td>


<td >{{$productcustompillar->end}}</td>

<td scope="row">
          <span class="
              @if($productcustompillar->status=='กำลังทำ')
                  badge badge-warning
              @elseif($productcustompillar->status=='เสร็จสิ้น')
                  badge badge-success
                @elseif($productcustompillar->status=='ยกเลิกงาน')
                  badge badge-danger
                  @else 
                  badge badge-info
              @endif
          ">{{($productcustompillar->status)}}</span>

</td>

<td>

<a href="{{ route('editWork',$productcustompillar->id)}}" class ="btn btn-info">มอบหมายงาน</a>
</td>

@endforeach
        </tbody>
        </table>
        
        {{$productcustompillars->links()}}

    </div>   
  

@endsection