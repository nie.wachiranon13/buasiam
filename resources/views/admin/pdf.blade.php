<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        html,
        body,
        table,
        td,
        th,
        p,
        span,
        div {
            font-family: "THSarabunNew";
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 18px;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>

<body>
    <h1>ใบแจ้งหนี้สำหรับ {{ "คุณ".$invoice->cus_name }}</h1>

    <div>
        <h3>เลขที่ใบแจ้งหนี้ : {{ $invoice->ref_id }}</h3>
    </div>

    <table>
        <tr>
            <th style="text-align: center;">รายการที่สั่ง</th>
            <th style="text-align: center;">จำนวน</th>
            <th style="text-align: center;">ราคา</th>
        </tr>

        @foreach($invoice_details as $item)
        <tr>
            <td>
                {{ $item->product->name }}
                {{ !empty($item->note) ? ' + '.$item->note : ''}}
            </td>
            <td style="text-align: right;">{{ number_format($item->amount) }} </td>
            <td style="text-align: right;">
                {{ !empty($item->other_price) ? number_format($item->other_price+$item->total_price) : number_format($item->total_price) }}
            </td>
        </tr>
        @endforeach
        <tr>
            <td colspan="2">รวม</td>
            <td style="text-align: right;">{{ number_format($invoice->totalprice) }}</td>
        </tr>
        <div style="text-align:center;font-size: 22px;">
            รวมเป็นเงิน <span style="text-align: center;">
                <strong>
                    {{ baht_text( $invoice->totalprice) }}
                </strong>
            </span>
        </div>

    </table>

</body>

</html>