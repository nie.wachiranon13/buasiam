@extends('layouts.admin')
<!-- ดึงlayoutของadminมา -->
@section('body')

<div class="table-responsive ">
    <h2>รายการสั่งซื้อ</h2>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">รหัสสั่งซื้อ</th>
                <th scope="col">วันที่สั่งซื้อ</th>

                <th scope="col">ราคา</th>


                <th scope="col">สถานะ</th>
                <th scope="col">รายละเอียดใบสั่งซื้อ</th>
                <th scope="col">ใบเสนอราคา</th>

            </tr>
        </thead>
        <tbody>

            @foreach($invoices as $invoice)
            <tr>
                <td scope="row">{{ $invoice->ref_id }}</td>
                <th scope="row">{{ DateThai($invoice->created_at) }}</th>
                <th scope="row">{{ number_format($invoice->totalprice) }}</th>
                <td scope="row">{{ $invoice->status }}</td>
                <td>
                    <a href="{{ route('order-custom.detail',$invoice->id) }}" class="btn btn-info">รายละเอียด</a>
                </td>
                <td>
                    @if($invoice->status != 'รอการยืนยัน')
                    <a target="_blank" href="{{ route('invoice.pdf',$invoice->id) }}" class="btn btn-primary">เปิดดูใบเสนอราคา</a>
                    @else
                    -
                    @endif
                </td>
            </tr>
            @endforeach


        </tbody>
    </table>

</div>



@endsection