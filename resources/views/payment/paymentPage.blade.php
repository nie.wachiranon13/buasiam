@extends("layouts.index")
@section("content")
<section id="cart_items">
<div class="container">
    <div class="breadcrumbs">
        <ol class="breadcrumb">
            <li><a href="/">หน้าแรก</a></li>
            <li class="active">ตะกร้าสินค้า</li>
        </ol>
    </div>
        <div class="shopper-informations">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <div class="bill-to">
                        <p> บิลชำระสินค้า</p>
                        <div class="form-one">
                       
                                       <div class="total_area" style="padding:10px">

                                       <ul>
                                       <li>สถานะการจายเงิน
                                      

                                       <span>{{$payment_info['status']}}</span>

                                   
                                       </li>
                                       <li>รวม
                                       <span>{{$payment_info['price']}}</span>
                                       </li>
                                       </ul>

                              
                                      </div>
                      

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <figure class="text-center"> 
   <figcaption class="figure-caption"></figcaption>
  <img src="{{asset('images/home/cheackbank.jpg')}}" class="figure-img img-fluid rounded" alt="...">
 <br>
 <a href="{{ route('showslip')}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">ส่งหลักฐานการชำระเงิน</a>
</figure> 

</div>
</section>

@endsection