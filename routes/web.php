<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/products', "ProductController@index");
Route::get('/products/category/{id}', 'ProductController@findCategory');

Route::get('/products/details/{id}', 'ProductController@details');



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
  return redirect('/products');
});

Auth::routes();



Route::middleware(['auth', 'verifyIsAdmin'])->group(function () {

  Route::get('invoice-pdf/{id}', 'Admin\InviceController@pdf')->name('invoice.pdf');

  //cat 
  Route::get('admin/createCategory', 'Admin\CategoryController@index')->name('admintest');
  Route::post('admin/createCategory', 'Admin\CategoryController@store');
  Route::get('admin/editCategory/{id}', 'Admin\CategoryController@edit');
  Route::post('admin/updateCategory/{id}', 'Admin\CategoryController@update');
  Route::get('admin/deleteCategory{id}', 'Admin\CategoryController@delete');

  //pro
  Route::get('admin/createProduct', 'Admin\ProductController@create');
  Route::get('admin/createCustom', 'Admin\ProductController@createCustom');
  Route::post('admin/createProduct', 'Admin\ProductController@store');
  Route::post('admin/createProductCustom', 'Admin\ProductController@storeCustom');
  Route::get('admin/dashboard', 'Admin\ProductController@index')->name('product.index');
  Route::get('admin/editProduct/{id}', 'Admin\ProductController@edit');
  Route::get('admin/editProductImage/{id}', 'Admin\ProductController@editImage');
  Route::post('admin/updateProduct/{id}', 'Admin\ProductController@update');
  Route::post('admin/updateProductImage/{id}', 'Admin\ProductController@updateImage');
  Route::get('admin/deleteProduct/{id}', 'Admin\ProductController@delete');


  //สั่งซื้อ
  Route::get('admin/orders', 'Admin\OrderController@orderPanel');
  Route::get('admin/orders/detail/{id}', 'Admin\OrderController@showOrderDetail');
  Route::get('admin/orderCustom', 'Admin\OrdercustomController@orderCustom')->name('orderCustom.index');
  Route::get('admin/orderCustomDetail/{id}', 'Admin\OrdercustomController@orderCustomDetail')->name('order-custom.detail');
  Route::post('admin/updateStatus', 'Admin\OrdercustomController@updateStatus')->name('updateStatusCus');
  Route::get('admin/editStatus/{id}', 'Admin\OrderController@edit');
  Route::post('admin/updateStatus/{id}', 'Admin\OrderController@updateStatus')->name('updateStatus');
  Route::get('admin/checkslip', 'Admin\OrderController@checkslip');


  // ปฏิทิน
  Route::get('admin/fullcalendareventmaster', 'FullCalendarEventMasterController@index')->name('index');
  Route::get('admin/load-events', 'EventController@loadEvents')->name('routeLoadEvents');

  Route::put('admin/event-update', 'EventController@update')->name('routeEventUpdate');

  Route::post('admin/fullcalendareventmaster/create', 'FullCalendarEventMasterController@create');

  Route::post('admin/fullcalendareventmaster/update', 'FullCalendarEventMasterController@update');

  Route::post('admin/fullcalendareventmaster/delete', 'FullCalendarEventMasterController@destroy');

  // สต๊อก
  Route::get('admin/stock', "Admin\StockController@showStock");
  Route::get('admin/addStock/{id}', "Admin\StockController@addStock")->name('addStockProduct');

  Route::post('admin/insertStock', 'Admin\StockController@insertStockDetail')->name('insertStock');
  Route::get('admin/showDeatailStock/{id}', 'Admin\StockController@showDeatailStock')->name('showStockDetail');

  //งาน
  Route::get('admin/showWork', "Admin\WorkController@showWork")->name('showWork');
  Route::get('admin/editWork/{id}', "Admin\WorkController@editWork")->name('editWork');
  Route::get('admin/addWork/{id}', "Admin\WorkController@addWork");
  Route::post('admin/updateWord', 'Admin\WorkController@updateWord')->name('updateWord');
  Route::post('admin/reworkWord', 'Admin\WorkController@reworkWord')->name('reworkWord');

  //วัตถุดิบ


  Route::get('admin/material', 'Admin\MaterialController@index')->name('material.index');

  Route::get('admin/MaterialForm', 'Admin\MaterialController@create');
  Route::post('admin/createMaterial', 'Admin\MaterialController@store');
  Route::get('admin/showDeatailStockMaterial/{id}', 'Admin\MaterialController@showDeatailStock')->name('showStockMaterialDetail');

  //สต๊อกวัตถุดิบ
  Route::get('admin/stockMaterial', "Admin\MaterialController@showStock");
  Route::get('admin/addStockMaterial/{id}', "Admin\MaterialController@addStock");
  Route::post('admin/insertStockMaterial', 'Admin\MaterialController@insertStockDetail')->name('insertStockMaterial');
  //หนักงาน


  Route::post('admin/updateEmp/{id}', 'Admin\EmployeeController@update')->name('updateEmp');
  Route::get('admin/editEmp/{id}', 'Admin\EmployeeController@edit');
  Route::get('admin/deleteEmp/{id}', 'Admin\EmployeeController@delete')->name('deleteEmp');

  Route::post('admin/createEmployee', 'Admin\EmployeeController@store');
  Route::get('admin/employeeForm', 'Admin\EmployeeController@create');
  Route::get('admin/Employee', "Admin\EmployeeController@show");
});
//รายได้
Route::get('admin/showSum', "Admin\SumController@showSum")->name('showSum');
Route::get('admin/showSumPillars', "Admin\SumController@showSumPillars")->name('showSumPillars');

//หน้าบ้าน
Route::middleware(['auth'])->group(function () {

  //addcart
  Route::get('/products/addToCart/{id}', 'ProductController@addProductToCart');
  Route::get('/products/cart', 'ProductController@showCart');
  Route::get('/products/cart/deleteFromCart/{id}', 'ProductController@deleteFromCart');
  Route::get('/products/cart/incrementCart/{id}', 'ProductController@incrementCart');
  Route::get('/products/cart/decrementCart/{id}', 'ProductController@decrementCart');
  Route::post('products/addQuantityToCart', 'ProductController@addQuantityToCart');

  //สร้างorder
  Route::get('/products/checkout', 'ProductController@checkout');
  Route::post('/products/createOrder', 'ProductController@createOrder');
  Route::get('/products/showPayment', 'ProductController@showPayment');
  Route::post('/products/sendslip', 'ProductController@sendslip');

  Route::get('products/confirmslip', 'ProductController@confirmslip')->name('confirmslip');


  //สินค้า0custom
  Route::get('products/createCustom', 'ProductController@createCustom');
  Route::post('products/createCustom', 'ProductController@store');
  Route::get('products/thank', 'ProductController@thank');

  Route::get('products/pillar', 'ProductController@createPillar');
  Route::post('products/storePillar', 'ProductController@storePillar');

  Route::get('products/cutebua', 'ProductController@createCutebua');
  Route::post('products/storeCutebua', 'ProductController@storeCutebua');

  Route::get('products/headpillar', 'ProductController@createHeadpillar');
  Route::post('products/storeHead', 'ProductController@storeHead');

  Route::get('products/create3d', 'ProductController@create3d');
  Route::post('products/store3d', 'ProductController@store3d');

  Route::get('products/test', 'ProductController@test');
  Route::post('products/test', 'ProductController@testshow');

  //cheackstatuspayment
  Route::get('products/checkstatuspayment', 'ProductController@checkstatuspayment');

  //ออเด้อร์หน้าบ้าน

  Route::get('product/orders/detail/{id}', 'ProductController@showOrderdetail')->name('orderdetailforauth');

  //สลิป

  Route::get('products/slip', 'ProductController@slip')->name('showslip');
  Route::get('products/sliporder/{id}', 'ProductController@slipOrder')->name('slipOrder');
  Route::get('products/slipcustom/{id}', 'ProductController@slipCustom')->name('slipCustom');
  Route::post('products/sendsliporder/{id}', 'ProductController@sendslipOrder')->name('sendslipOrder');
  Route::post('products/sendslipcustom/{id}', 'ProductController@sendslipCustom')->name('sendslipCustom');
  Route::get('products/orderCustomDetail/{id}', 'OrdercustomController@orderCustomDetail')->name('ordercustom.detail');




  // ขอบคุฯ
  Route::get('products/thank', 'ProductController@thank');
});
Route::get('/products/search', 'ProductController@searchProduct');

Route::post('get-order-data', 'Admin\DatatablesController@orderData')->name('datatables.order');
Route::post('get-order-total', 'Admin\DatatablesController@total')->name('datatables.total');

Route::post('get-order-data-pillars', 'Admin\DatatablesController@orderDataPillars')->name('datatables.orderPillars');
Route::post('get-order-total-pillars', 'Admin\DatatablesController@totalPillars')->name('datatables.totalPillars');

//ปริ้นใบเสร็จ
Route::get('products/orderitems', 'PrintController@index');
Route::get('products/prnpriview', 'PrintController@prnpriview');

//pdf
Route::get('products/PDF', 'PDFMaker@gen');


Route::post('/custom_order_store', 'ProductController@orderCustomStore')
  ->name('custom_order.store');


Route::get('products/createInvoice', 'ProductController@createInvoice')->name('invoice');
