<?php

use Illuminate\Database\Seeder;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('events')->insert([
            [
                'title' => 'งานปูน',
                'start' => '2020-05-27 21:30:00',
                'end' => '2020-05-30 21:30:00',
                'color' => '#33CCFF',
                'description' => '20 ซม.'
            ],
            [
                'title' => 'งานปั้น',
                'start' => '2020-05-02',
                'end' => '2020-05-03',
                'color' => '#29fdf2',
                'description' => 'Falar com cliente'
            ]
        ]);
    }
}