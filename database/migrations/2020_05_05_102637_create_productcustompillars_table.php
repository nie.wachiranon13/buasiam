<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductcustompillarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productcustompillars', function (Blueprint $table) {
            $table->bigIncrements('custom_id');
            $table->string('custom_name',30);
            $table->integer('custom_tel');
            $table->text('custom_description');
            $table->string('custom_image');
            $table->decimal('custom_price',8,2);
            $table->integer('custom_length');
            $table->integer('custom_width');
            $table->integer('custom_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productcustompillars');
    }
    }
